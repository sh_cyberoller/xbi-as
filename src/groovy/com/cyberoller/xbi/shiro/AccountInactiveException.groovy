package com.cyberoller.xbi.shiro

import org.apache.shiro.authc.AuthenticationException


class AccountInactiveException extends AuthenticationException{
	public AccountInactiveException(String message){
        super(message)
    }
}