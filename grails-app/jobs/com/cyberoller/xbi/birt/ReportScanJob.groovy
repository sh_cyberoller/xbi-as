package com.cyberoller.xbi.birt

import com.cyberoller.xbi.quartz2.QuartzJob

class ReportScanJob {

	def reportScanService
	
	def concurrent = false
    
	static triggers = {
		cron name: 'ReportScanTrigger', group: 'ReportScanGroup', cronExpression: "0,15,30,45 * * * * ?"
	}
	
	def execute() {
		def isActive = QuartzJob.withCriteria(uniqueResult:true){
			projections{
				property("isActive")
			}
			eq("name", "ReportScanTrigger")
		}

		if(isActive == true){
			//reportScanService.scan()
		}
	}
}
