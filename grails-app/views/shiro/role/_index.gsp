<%@ page import="com.cyberoller.xbi.shiro.Role" %>

<table>
<thead>
		<tr>
			<g:sortableColumn property="name" title="${message(code: 'role.name.label', default: 'Name')}" />
			<g:sortableColumn property="alias" title="${message(code: 'role.alias.label', default: 'Alias')}" />
			<g:sortableColumn property="description" title="${message(code: 'role.description.label', default: 'Description')}" />
			<g:sortableColumn property="dateCreated" title="${message(code: 'role.dateCreated.label', default: 'Date Created')}" />
			<g:sortableColumn property="lastUpdated" title="${message(code: 'role.lastUpdated.label', default: 'Last Updated')}" />
		</tr>
	</thead>
	<tbody>
	<g:each in="${roleInstanceList}" status="i" var="roleInstance">
		<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
			<td><g:link action="show" id="${roleInstance.id}">${fieldValue(bean: roleInstance, field: "name")}</g:link></td>
			<td>${fieldValue(bean: roleInstance, field: "alias")}</td>
			<td>${fieldValue(bean: roleInstance, field: "description")}</td>
			<td><g:formatDate date="${roleInstance.dateCreated}" /></td>
			<td><g:formatDate date="${roleInstance.lastUpdated}" /></td>
		</tr>
	</g:each>
	</tbody>
</table>
<div class="pagination">
	<g:paginate total="${roleInstanceCount ?: 0}" />
</div>