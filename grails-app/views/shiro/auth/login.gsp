<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="login" />
	<title>
		<g:message code="login.page.title" default="Login" />
		~
		<g:message code="global.app.name" default="XBI"/>
	</title>
	
	<script type="text/javascript">
		function resize(){
			//浏览器时下窗口可视区域高度
			$(".login").css("min-height",$(window).height().toString() + "px");
		}
		$(window).resize(function() {
			resize();
		});
		$(document).ready(function(){
			resize();
			//用户名获取焦点
			$("input[name='username']").focus();
		});
	</script>
</head>
<body>

	<div class="login">

		<div class="login-screen">
		
			<div class="page-header">
			  <h4>
			  	<g:message code="login.page.title" default="Login" />
			  	~
			  	<g:message code="global.app.name" default="XBI"/>
			  </h4>
			</div>
		
			<g:if test="${flash.message}">
				<div class="alert alert-danger">
					<g:message code="${flash.message}" args="${flash.args}" default="${flash.default}"/>
				</div>
			</g:if>
		
			<g:form action="signIn">
			
				<input type="hidden" name="targetUri" value="${targetUri}" />
				<input type="hidden" name="rememberMe" value="${true}" />
						
				<div class="login-form">
					<div class="form-group">
					<input type="text" class="form-control login-field" value="${username}" placeholder="${message(code:'login.page.username', default : 'Username')}" id="username" name="username" />
					<label class="login-field-icon fui-user" for="login-name"></label>
				</div>

				<div class="form-group">
					<input type="password" class="form-control login-field" value="" placeholder="${message(code:'login.page.password', default : 'Password')}" id="password" name="password" />
					<label class="login-field-icon fui-lock" for="login-pass"></label>
				</div>

				<button type="submit" class="btn btn-primary">
					<g:message code="login.page.signIn" default="Sign in" />
				</button>
			</g:form>
		</div>
	</div>
	
</body>
</html>