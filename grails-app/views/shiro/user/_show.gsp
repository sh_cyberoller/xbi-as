<%@ page import="com.cyberoller.xbi.shiro.User" %>

<ol class="property-list user">

	<g:if test="${userInstance?.username}">
	<li class="fieldcontain">
		<span id="username-label" class="property-label"><g:message code="user.username.label" default="Username" /></span>
		<span class="property-value" aria-labelledby="username-label"><g:fieldValue bean="${userInstance}" field="username"/></span>
	</li>
	</g:if>

	<g:if test="${userInstance?.fullname}">
	<li class="fieldcontain">
		<span id="fullname-label" class="property-label"><g:message code="user.fullname.label" default="Fullname" /></span>
		<span class="property-value" aria-labelledby="fullname-label"><g:fieldValue bean="${userInstance}" field="fullname"/></span>
	</li>
	</g:if>

	<g:if test="${userInstance?.roleAlias}">
	<li class="fieldcontain">
		<span id="roleAlias-label" class="property-label"><g:message code="user.roleAlias.label" default="Role Alias" /></span>
		<span class="property-value" aria-labelledby="roleAlias-label"><g:fieldValue bean="${userInstance}" field="roleAlias"/></span>
	</li>
	</g:if>

	<g:if test="${userInstance?.language}">
	<li class="fieldcontain">
		<span id="language-label" class="property-label"><g:message code="user.language.label" default="Language" /></span>
		<span class="property-value" aria-labelledby="language-label"><g:fieldValue bean="${userInstance}" field="language"/></span>
	</li>
	</g:if>

	<g:if test="${userInstance?.isActive}">
	<li class="fieldcontain">
		<span id="isActive-label" class="property-label"><g:message code="user.isActive.label" default="Is Active" /></span>
		<span class="property-value" aria-labelledby="isActive-label"><g:formatBoolean boolean="${userInstance?.isActive}" /></span>
	</li>
	</g:if>

	<g:if test="${userInstance?.createdBy}">
	<li class="fieldcontain">
		<span id="createdBy-label" class="property-label"><g:message code="user.createdBy.label" default="Created By" /></span>
			<span class="property-value" aria-labelledby="createdBy-label"><g:link controller="user" action="show" id="${userInstance?.createdBy?.id}">${userInstance?.createdBy?.encodeAsHTML()}</g:link></span>
		
	</li>
	</g:if>

	<g:if test="${userInstance?.lastModifiedBy}">
	<li class="fieldcontain">
		<span id="lastModifiedBy-label" class="property-label"><g:message code="user.lastModifiedBy.label" default="Last Modified By" /></span>
		<span class="property-value" aria-labelledby="lastModifiedBy-label">
			<g:link controller="user" action="show" id="${userInstance?.lastModifiedBy?.id}">
				${userInstance?.lastModifiedBy?.encodeAsHTML()}
			</g:link>
		</span>
	</li>
	</g:if>

	<g:if test="${userInstance?.roles}">
	<li class="fieldcontain">
		<span id="roles-label" class="property-label"><g:message code="user.roles.label" default="Roles" /></span>
		<g:each in="${userInstance.roles}" var="r">
			<span class="property-value" aria-labelledby="roles-label"><g:link controller="role" action="show" id="${r.id}">${r?.encodeAsHTML()}</g:link></span>
		</g:each>
	</li>
	</g:if>

	<g:if test="${userInstance?.permissions}">
	<li class="fieldcontain">
		<span id="permissions-label" class="property-label"><g:message code="user.permissions.label" default="Permissions" /></span>
		<span class="property-value" aria-labelledby="permissions-label"><g:fieldValue bean="${userInstance}" field="permissions"/></span>
	</li>
	</g:if>


	<g:if test="${userInstance?.dateCreated}">
	<li class="fieldcontain">
		<span id="dateCreated-label" class="property-label"><g:message code="user.dateCreated.label" default="Date Created" /></span>
		<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${userInstance?.dateCreated}" /></span>
	</li>
	</g:if>

	<g:if test="${userInstance?.lastUpdated}">
	<li class="fieldcontain">
		<span id="lastUpdated-label" class="property-label"><g:message code="user.lastUpdated.label" default="Last Updated" /></span>
		<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${userInstance?.lastUpdated}" /></span>
	</li>
	</g:if>

</ol>