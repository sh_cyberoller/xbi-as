<%@ page import="com.cyberoller.xbi.shiro.User" %>

<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'username', 'error')} required">
	<label for="username">
		<g:message code="user.username.label" default="Username" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="username" maxlength="20" required="" value="${userInstance?.username}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'fullname', 'error')} required">
	<label for="fullname">
		<g:message code="user.fullname.label" default="Fullname" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="fullname" maxlength="35" required="" value="${userInstance?.fullname}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'roleAlias', 'error')} required">
	<label for="roleAlias">
		<g:message code="user.roleAlias.label" default="Role Alias" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="roleAlias" maxlength="3" required="" value="${userInstance?.roleAlias}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'language', 'error')} required">
	<label for="language">
		<g:message code="user.language.label" default="Language" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="language" maxlength="10" required="" value="${userInstance?.language}"/>
</div>