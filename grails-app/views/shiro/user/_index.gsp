<%@ page import="com.cyberoller.xbi.shiro.User" %>
<table>
	<thead>
		<tr>
			<g:sortableColumn property="username" title="${message(code: 'user.username.label', default: 'Username')}" />
			<g:sortableColumn property="fullname" title="${message(code: 'user.fullname.label', default: 'Fullname')}" />
			<g:sortableColumn property="roleAlias" title="${message(code: 'user.roleAlias.label', default: 'Role Alias')}" />
			<g:sortableColumn property="language" title="${message(code: 'user.language.label', default: 'Language')}" />
			<g:sortableColumn property="language" title="${message(code: 'user.isActive.label', default: 'Active?')}" />
			<g:sortableColumn property="dateCreated" title="${message(code: 'user.dateCreated.label', default: 'Date Created')}" />
			<g:sortableColumn property="lastUpdated" title="${message(code: 'user.lastUpdated.label', default: 'Last Updated')}" />
		</tr>
	</thead>
	<tbody>
	<g:each in="${userInstanceList}" status="i" var="userInstance">
		<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
			<td><g:link action="show" id="${userInstance.id}">${fieldValue(bean: userInstance, field: "username")}</g:link></td>
			<td>${fieldValue(bean: userInstance, field: "fullname")}</td>
			<td>${fieldValue(bean: userInstance, field: "roleAlias")}</td>
			<td>${fieldValue(bean: userInstance, field: "language")}</td>
			<td><g:formatBoolean boolean="${userInstance?.isActive}" /></td>
			<td><g:formatDate date="${userInstance?.dateCreated}"/></td>
			<td><g:formatDate date="${userInstance?.lastUpdated}"/></td>
		</tr>
	</g:each>
	</tbody>
</table>
<div class="pagination">
	<g:paginate total="${userInstanceCount ?: 0}" />
</div>