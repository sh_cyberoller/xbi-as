<%@ page import="com.cyberoller.xbi.Xfolder" %>
<table>
<thead>
		<tr>
			<g:sortableColumn property="serialNumber" title="${message(code: 'xfolder.serialNumber.label', default: 'Serial Number')}" />
			<g:sortableColumn property="name" title="${message(code: 'xfolder.name.label', default: 'Name')}" />
			<g:sortableColumn property="description" title="${message(code: 'xfolder.description.label', default: 'Description')}" />
			<th><g:message code="xfolder.owner.label" default="Owner" /></th>
			<th><g:message code="xfolder.createdBy.label" default="Created By" /></th>
			<th><g:message code="xfolder.lastModifiedBy.label" default="Last Modified By" /></th>
		</tr>
	</thead>
	<tbody>
	<g:each in="${xfolderInstanceList}" status="i" var="xfolderInstance">
		<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
			<td>
				<g:link action="show" id="${xfolderInstance.id}">
					${fieldValue(bean: xfolderInstance, field: "serialNumber")}
				</g:link>
				&nbsp;
				<g:link controller="xfolder" action="create" params="[xfolder:xfolderInstance.id]">
					(+)
				</g:link>
			</td>
			<td>
				<g:if test="${xfolderInstance.xfolder}">
						${include(action:'fullpath', id : xfolderInstance.id)}
				</g:if>
				<g:else>
						/${fieldValue(bean: xfolderInstance, field: "name")}
				</g:else>
			</td>
			<td>${fieldValue(bean: xfolderInstance, field: "description")}</td>
			<td>${fieldValue(bean: xfolderInstance, field: "owner")}</td>
			<td>
				${fieldValue(bean: xfolderInstance, field: "createdBy")}, ${formatDate(date:xfolderInstance?.dateCreated)}
			</td>
			<td>
				${fieldValue(bean: xfolderInstance, field: "lastModifiedBy")}, ${formatDate(date:xfolderInstance?.lastUpdated)}
			</td>
		</tr>
	</g:each>
	</tbody>
</table>
<div class="pagination">
	<g:paginate total="${XFolderInstanceCount ?: 0}" />
</div>