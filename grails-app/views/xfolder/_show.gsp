<%@ page import="com.cyberoller.xbi.Xfolder" %>
<ol class="property-list xfolder">

	<g:if test="${xfolderInstance?.xfolder}">
	<li class="fieldcontain">
		<span id="xfolder-label" class="property-label"><g:message code="xfolder.xfolder.label" default="Xfolder" /></span>
		<span class="property-value" aria-labelledby="xfolder-label">
			<g:link controller="xfolder" action="show" id="${xfolderInstance?.xfolder?.id}">
				${include(action:'fullpath', id : xfolderInstance.id)}
			</g:link>
		</span>
	</li>
	</g:if>

	<g:if test="${xfolderInstance?.serialNumber}">
	<li class="fieldcontain">
		<span id="serialNumber-label" class="property-label"><g:message code="xfolder.serialNumber.label" default="Serial Number" /></span>
		<span class="property-value" aria-labelledby="serialNumber-label"><g:fieldValue bean="${xfolderInstance}" field="serialNumber"/></span>
	</li>
	</g:if>

	<g:if test="${xfolderInstance?.name}">
	<li class="fieldcontain">
		<span id="name-label" class="property-label"><g:message code="xfolder.name.label" default="Name" /></span>
		<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${xfolderInstance}" field="name"/></span>
	</li>
	</g:if>

	<g:if test="${xfolderInstance?.description}">
	<li class="fieldcontain">
		<span id="description-label" class="property-label"><g:message code="xfolder.description.label" default="Description" /></span>
		<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${xfolderInstance}" field="description"/></span>
	</li>
	</g:if>

	<g:if test="${xfolderInstance?.owner}">
	<li class="fieldcontain">
		<span id="owner-label" class="property-label"><g:message code="xfolder.owner.label" default="Owner" /></span>
		<span class="property-value" aria-labelledby="owner-label">
			<g:link controller="user" action="show" id="${xfolderInstance?.owner?.id}">${xfolderInstance?.owner?.encodeAsHTML()}</g:link>
		</span>
	</li>
	</g:if>

	<g:if test="${xfolderInstance?.createdBy}">
	<li class="fieldcontain">
		<span id="createdBy-label" class="property-label"><g:message code="xfolder.createdBy.label" default="Created By" /></span>
		<span class="property-value" aria-labelledby="createdBy-label">
			<g:link controller="user" action="show" id="${xfolderInstance?.createdBy?.id}">${xfolderInstance?.createdBy?.encodeAsHTML()}</g:link>
		</span>
	</li>
	</g:if>

	<g:if test="${xfolderInstance?.lastModifiedBy}">
	<li class="fieldcontain">
		<span id="lastModifiedBy-label" class="property-label"><g:message code="xfolder.lastModifiedBy.label" default="Last Modified By" /></span>
		<span class="property-value" aria-labelledby="lastModifiedBy-label">
			<g:link controller="user" action="show" id="${xfolderInstance?.lastModifiedBy?.id}">${xfolderInstance?.lastModifiedBy?.encodeAsHTML()}</g:link>
		</span>
	</li>
	</g:if>

	<g:if test="${xfolderInstance?.dateCreated}">
	<li class="fieldcontain">
		<span id="dateCreated-label" class="property-label"><g:message code="xfolder.dateCreated.label" default="Date Created" /></span>
		<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${xfolderInstance?.dateCreated}" /></span>
	</li>
	</g:if>

	<g:if test="${xfolderInstance?.lastUpdated}">
	<li class="fieldcontain">
		<span id="lastUpdated-label" class="property-label"><g:message code="xfolder.lastUpdated.label" default="Last Updated" /></span>
		<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${xfolderInstance?.lastUpdated}" /></span>
	</li>
	</g:if>


</ol>

<table>
	<caption>
		<g:message code="xfolder.xfiles.label" default="Xfiles" />
		&nbsp;
		<g:link controller="xfile" action="create" params="[xfolder:xfolderInstance.id]">(+)</g:link>
	</caption>
	<thead>
		<tr>
			<th><g:message code="xfile.serialNumber.label" default="Serial Number" /></th>
			<th><g:message code="xfile.xtype.label" default="Type" /></th>
			<th>
				<g:message code="xfile.report.label" default="Report" />
				/
				<g:message code="xfile.dashboard.label" default="Dashboard" />
			</th>
			<th><g:message code="xfile.owner.label" default="Owner" /></th>
			<th><g:message code="xfile.createdBy.label" default="Created By" /></th>
			<th><g:message code="xfile.lastModifiedBy.label" default="Last Modified By" /></th>
		</tr>
	</thead>
	<tbody>
	<g:each in="${xfolderInstance.xfiles?.sort{return it.serialNumber}}" status="i" var="xfileInstance">
		<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
			<td>
				<g:link controller="xfile" action="show" id="${xfileInstance.id}">
					${fieldValue(bean: xfileInstance, field: "serialNumber")}
				</g:link>
			</td>
			<td>${fieldValue(bean: xfileInstance, field: "xtype")}</td>
			<td>
				<g:if test="${xfileInstance?.xtype == 'Report'}">
					${fieldValue(bean: xfileInstance, field: "report")}
				</g:if>
				<g:if test="${xfileInstance?.xtype == 'Dashboard'}">
					${fieldValue(bean: xfileInstance, field: "dashboard")}
				</g:if>
			</td>
			<td>${fieldValue(bean: xfileInstance, field: "owner")}</td>
			<td>
				${fieldValue(bean: xfileInstance, field: "createdBy")}
				,
				${formatDate(date:xfileInstance?.dateCreated)}
			</td>
			<td>
				${fieldValue(bean: xfileInstance, field: "lastModifiedBy")}
				,
				${formatDate(date:xfileInstance?.lastUpdated)}	
			</td>
		</tr>
	</g:each>
	</tbody>
</table>
