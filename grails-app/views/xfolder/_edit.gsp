<%@ page import="com.cyberoller.xbi.Xfolder" %>

<div class="fieldcontain ${hasErrors(bean: xfolderInstance, field: 'xfolder', 'error')} ">
	<label for="xfolder">
		<g:message code="xfolder.xfolder.label" default="Xfolder" />
	</label>
	<g:if test="${fullpath}">
		${fullpath}
	</g:if>
	<g:else>
		${xfolderInstance?.xfolder?.name}
	</g:else>
	<g:hiddenField name="xfolder.id" value="${xfolderInstance?.xfolder?.id}" />
</div>

<div class="fieldcontain ${hasErrors(bean: xfolderInstance, field: 'serialNumber', 'error')} required">
	<label for="serialNumber">
		<g:message code="xfolder.serialNumber.label" default="Serial Number" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="serialNumber" maxlength="4" required="" value="${xfolderInstance?.serialNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: xfolderInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="xfolder.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" maxlength="50" required="" value="${xfolderInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: xfolderInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="xfolder.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="200" value="${xfolderInstance?.description}"/>
</div>