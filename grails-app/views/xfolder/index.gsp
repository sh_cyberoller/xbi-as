<%@ page import="com.cyberoller.xbi.Xfolder" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">

		<g:set var="entityName" value="${message(code: 'xfolder.label', default: 'Folder')}" />

		<title>
			<g:message code="default.list.label" args="[entityName]" />
			~
			<g:message code="global.app.name" />
		</title>

	</head>
	<body>
		<a href="#list-Xfolder" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-Xfolder" class="content scaffold-list" role="main">
			
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			
			<g:render template="/xfolder/index" />

		</div>
	</body>
</html>
