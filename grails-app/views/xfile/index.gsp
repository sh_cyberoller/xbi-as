<%@ page import="com.cyberoller.xbi.Xfile" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">

		<g:set var="entityName" value="${message(code: 'xfile.label', default: 'XFile')}" />

		<title>
			<g:message code="default.list.label" args="[entityName]" />
			~
			<g:message code="global.app.name" />
		</title>
	</head>
	<body>
		<a href="#list-XFile" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-XFile" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			
			<g:render template="/xfile/index" />

		</div>
	</body>
</html>
