<%@ page import="com.cyberoller.xbi.Xfile" %>
<ol class="property-list XFile">

	<g:if test="${xfileInstance?.xfolder}">
	<li class="fieldcontain">
		<span id="xfolder-label" class="property-label"><g:message code="xfile.xfolder.label" default="Xfolder" /></span>
		<span class="property-value" aria-labelledby="xfolder-label">
			<g:link controller="xfolder" action="show" id="${xfileInstance?.xfolder?.id}">
				${include(controller:'xfolder', action:'fullpath', id : xfileInstance?.xfolder?.id)}
			</g:link>
		</span>
	</li>
	</g:if>

	<g:if test="${xfileInstance?.serialNumber}">
	<li class="fieldcontain">
		<span id="serialNumber-label" class="property-label"><g:message code="xfile.serialNumber.label" default="Serial Number" /></span>
		<span class="property-value" aria-labelledby="serialNumber-label"><g:fieldValue bean="${xfileInstance}" field="serialNumber"/></span>
	</li>
	</g:if>

	<g:if test="${xfileInstance?.xtype}">
	<li class="fieldcontain">
		<span id="xtype-label" class="property-label"><g:message code="xfile.xtype.label" default="Xtype" /></span>
		<span class="property-value" aria-labelledby="xtype-label"><g:fieldValue bean="${xfileInstance}" field="xtype"/></span>
	</li>
	</g:if>

	<g:if test="${xfileInstance?.report}">
	<li class="fieldcontain">
		<span id="report-label" class="property-label"><g:message code="xfile.report.label" default="Report" /></span>
		<span class="property-value" aria-labelledby="report-label">
			<g:link controller="report" action="show" id="${xfileInstance.report.id}">
				<g:fieldValue bean="${xfileInstance}" field="report"/>
			</g:link>
		</span>
	</li>
	</g:if>

	<g:if test="${xfileInstance?.dashboard}">
	<li class="fieldcontain">
		<span id="dashboard-label" class="property-label"><g:message code="xfile.dashboard.label" default="Dashboard" /></span>
		<span class="property-value" aria-labelledby="dashboard-label">
			<g:link controller="dashboard" action="show" id="${xfileInstance.dashboard.id}">
				<g:fieldValue bean="${xfileInstance}" field="dashboard"/>
			</g:link>
		</span>
	</li>
	</g:if>

	<g:if test="${xfileInstance?.owner}">
	<li class="fieldcontain">
		<span id="owner-label" class="property-label"><g:message code="xfile.owner.label" default="Owner" /></span>
		<span class="property-value" aria-labelledby="owner-label">
			<g:link controller="user" action="show" id="${xfileInstance?.owner?.id}">${xfileInstance?.owner?.encodeAsHTML()}</g:link>
		</span>
	</li>
	</g:if>

	<g:if test="${xfileInstance?.createdBy}">
	<li class="fieldcontain">
		<span id="createdBy-label" class="property-label"><g:message code="xfile.createdBy.label" default="Created By" /></span>
		<span class="property-value" aria-labelledby="createdBy-label">
			<g:link controller="user" action="show" id="${xfileInstance?.createdBy?.id}">${xfileInstance?.createdBy?.encodeAsHTML()}</g:link>
		</span>
	</li>
	</g:if>

	<g:if test="${xfileInstance?.lastModifiedBy}">
	<li class="fieldcontain">
		<span id="lastModifiedBy-label" class="property-label"><g:message code="xfile.lastModifiedBy.label" default="Last Modified By" /></span>
		<span class="property-value" aria-labelledby="lastModifiedBy-label">
			<g:link controller="user" action="show" id="${xfileInstance?.lastModifiedBy?.id}">${xfileInstance?.lastModifiedBy?.encodeAsHTML()}</g:link>
		</span>
	</li>
	</g:if>

	<g:if test="${xfileInstance?.dateCreated}">
	<li class="fieldcontain">
		<span id="dateCreated-label" class="property-label"><g:message code="xfile.dateCreated.label" default="Date Created" /></span>
		<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${xfileInstance?.dateCreated}" /></span>
	</li>
	</g:if>

	<g:if test="${xfileInstance?.lastUpdated}">
	<li class="fieldcontain">
		<span id="lastUpdated-label" class="property-label"><g:message code="xfile.lastUpdated.label" default="Last Updated" /></span>
		<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${xfileInstance?.lastUpdated}" /></span>
	</li>
	</g:if>

</ol>