<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'xfile.label', default: 'XFile')}" />
		<title>
			<g:message code="default.create.label" args="[entityName]" />
			~
			<g:message code="global.app.name" />
		</title>
	</head>
	<body>
		<a href="#create-XFile" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="create-XFile" class="content scaffold-create" role="main">

			<h1><g:message code="default.create.label" args="[entityName]" /></h1>

			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>

			<g:hasErrors bean="${xfileInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${xfileInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>

			<g:form url="[resource:xfileInstance, action:'save']" >

				<fieldset class="form">
					<g:hiddenField name="owner.id" value="${currentUserId}" />
					<g:hiddenField name="createdBy.id" value="${currentUserId}" />
					<g:hiddenField name="lastModifiedBy.id" value="${currentUserId}" />
					<g:render template="/xfile/edit" />
				</fieldset>

				<fieldset class="buttons">
					<g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
				</fieldset>

			</g:form>
		</div>
	</body>
</html>
