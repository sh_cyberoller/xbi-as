<%@ page import="com.cyberoller.xbi.Xfile" %>
<table>
<thead>
		<tr>
			<th><g:message code="xfile.xfolder.label" default="Folder" /></th>
			<g:sortableColumn property="serialNumber" title="${message(code: 'xfile.serialNumber.label', default: 'Serial Number')}" />
			<g:sortableColumn property="xtype" title="${message(code: 'xfile.xtype.label', default: 'Xtype')}" />
			<th><g:message code="xfile.owner.label" default="Owner" /></th>
			<th><g:message code="xfile.createdBy.label" default="Created By" /></th>
			<th><g:message code="xfile.lastModifiedBy.label" default="Last Modified By" /></th>
		</tr>
	</thead>
	<tbody>
	<g:each in="${xfileInstanceList}" status="i" var="xfileInstance">
		<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
			<td><g:link action="show" id="${xfileInstance.id}">${fieldValue(bean: xfileInstance, field: "xfolder")}</g:link></td>
			<td>${fieldValue(bean: xfileInstance, field: "serialNumber")}</td>
			<td>${fieldValue(bean: xfileInstance, field: "xtype")}</td>
			<td>${fieldValue(bean: xfileInstance, field: "owner")}</td>
			<td>${fieldValue(bean: xfileInstance, field: "createdBy")}</td>
			<td>${fieldValue(bean: xfileInstance, field: "lastModifiedBy")}</td>
		</tr>
	</g:each>
	</tbody>
</table>
<div class="pagination">
	<g:paginate total="${xfileInstanceCount ?: 0}" />
</div>