<%@ page import="com.cyberoller.xbi.Xfile" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'xfile.label', default: 'XFile')}" />
		<title>
			<g:message code="default.show.label" args="[entityName]" />
			~
			<g:message code="global.app.name" />
		</title>
	</head>
	<body>
		<a href="#show-XFile" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="show-XFile" class="content scaffold-show" role="main">

			<h1><g:message code="default.show.label" args="[entityName]" /></h1>

			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>

			<g:render template="/xfile/show" />

			<g:form url="[resource:xfileInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">

					<g:link class="edit" action="edit" resource="${xfileInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>

					<g:actionSubmit class="delete" action="delete" 
						value="${message(code: 'default.button.delete.label', default: 'Delete')}" 
						onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
