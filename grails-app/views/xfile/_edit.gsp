<%@ page import="com.cyberoller.xbi.Xfile" %>

<div class="fieldcontain ${hasErrors(bean: xfileInstance, field: 'xfolder', 'error')} ">
	<label for="xfolder">
		<g:message code="xfile.xfolder.label" default="Folder" />
	</label>
	<g:if test="${xfileInstance?.xfolder?.id}">
		<g:link controller="xfolder" action="show" id="${xfileInstance?.xfolder?.id}">
			${include(controller:'xfolder', action:'fullpath', id :xfileInstance?.xfolder?.id )}
		</g:link>
	</g:if>
	<g:hiddenField name="xfolder.id" value="${xfileInstance?.xfolder?.id}" />
</div>

<div class="fieldcontain ${hasErrors(bean: xfileInstance, field: 'serialNumber', 'error')} ">
	<label for="serialNumber">
		<g:message code="xfile.serialNumber.label" default="Serial Number" />
	</label>
	<g:textField name="serialNumber" maxlength="4" value="${xfileInstance?.serialNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: xfileInstance, field: 'xtype', 'error')} required">
	<label for="xtype">
		<g:message code="xfile.xtype.label" default="Xtype" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="xtype" from="${xfileInstance.constraints.xtype.inList}" required="" value="${xfileInstance?.xtype}" valueMessagePrefix="xfile.xtype"/>
</div>

<div class="fieldcontain ${hasErrors(bean: xfileInstance, field: 'report', 'error')}">
	<label for="report">
		<g:message code="xfile.report.label" default="Report" />
	</label>
	<g:select name="report" from="${com.cyberoller.xbi.birt.Report.list()}" optionKey="id" value="${xfileInstance?.report?.id}" class="many-to-one" noSelection="['':'']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: xfileInstance, field: 'dashboard', 'error')}">
	<label for="dashboard">
		<g:message code="xfile.dashboard.label" default="Dashboard" />
	</label>
	<g:select name="dashboard" from="${com.cyberoller.xbi.chartjs.Dashboard.list()}" optionKey="id" value="${xfileInstance?.dashboard?.id}" class="many-to-one" noSelection="['':'']"/>
</div>