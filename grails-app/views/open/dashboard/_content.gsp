<script type="text/javascript">
	var baseUrl = "${createLink( controller:'dashboard', action:'data', params:[id : dashboardInstance?.id])}";
	$(document).ready(function(){
		$.ajax({
			url: "${createLink( controller:'dashboard', action:'template', params:[id : dashboardInstance?.id])}"
		}).done(function(html){
			$("#dashboard_template").html(html)
			${dashboardInstance?.name}();
		});
	});
</script>

<div class="page-header">
	<g:if test="${errorMessage}">
		<div class="message" role="status">${errorMessage}</div>
	</g:if>
</div>

<div class="page-content">
	<div id="dashboard_template">
	
	</div>
</div>