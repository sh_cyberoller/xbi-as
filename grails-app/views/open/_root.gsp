<g:if test="${rootInstance}">


<ul id="browser" class="filetree">
	<li>
		<span class="folder">${rootInstance?.name}</span>
		<ul>
			<g:include controller="open" action="folders" id="${rootInstance?.id}" />
		</ul>
	</li>
</ul>
</g:if>