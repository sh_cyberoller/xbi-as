<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="user"/>
		<title>
			<g:message code="global.welcome.message" default="Welcome to "/>
			~
			<g:message code="global.app.name" default="XBI" />
		</title>
		
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'jquery.treeview.css')}" type="text/css">
		<g:javascript src="jquery.treeview.js"></g:javascript>
		<script type="text/javascript">
			$(function() {
				$("#browser").treeview();
				$("#browser").bind("contextmenu", function(event) {
					if ($(event.target).is("li") || $(event.target).parents("li").length) {
						$("#browser").treeview({
							remove : $(event.target).parents("li").filter(":first")
						});
						return false;
					}
				});
			})
		</script>
	</head>
	<body>
		<div id="list-Xfile" class="content scaffold-list" role="main">
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			
			<g:include action="rootFolder" />
			
		</div>
	</body>
</html>
