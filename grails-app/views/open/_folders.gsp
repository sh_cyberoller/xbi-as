<g:if test="${folderInstanceList}">
	<g:each in="${folderInstanceList}" status="i" var="folderInstance">
	<li>
		<span class="folder">${folderInstance?.name}</span>
		<ul>
			<g:include controller="open" action="files" id="${folderInstance?.id}" />
		</ul>
	</li>
	</g:each>
</g:if>