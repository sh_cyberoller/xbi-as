<g:if test="${fileInstanceList}">
	<g:each in="${fileInstanceList}" status="i" var="fileInstance">
	<li>
		<span class="file">
			<g:if test="${fileInstance.xtype == 'Report'}">
				<g:link controller="birt" action="run" id="${fileInstance.report}">
					${fileInstance.reportTitle}(R)
				</g:link>
			</g:if>
			<g:if test="${fileInstance.xtype == 'Dashboard'}">
				<g:link controller="open" action="dashboard" id="${fileInstance.dashboard}">
					${fileInstance.dashboardTitle}(D)
				</g:link>
			</g:if>
		</span>
	</li>
	</g:each>
</g:if>