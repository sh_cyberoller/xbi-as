<%@ page import="com.cyberoller.xbi.chartjs.DataSource" %>

<div class="fieldcontain ${hasErrors(bean: dataSourceInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="dataSource.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" maxlength="20" required="" value="${dataSourceInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: dataSourceInstance, field: 'url', 'error')} required">
	<label for="url">
		<g:message code="dataSource.url.label" default="Url" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="url" maxlength="200" required="" value="${dataSourceInstance?.url}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: dataSourceInstance, field: 'username', 'error')} required">
	<label for="username">
		<g:message code="dataSource.username.label" default="Username" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="username" maxlength="20" required="" value="${dataSourceInstance?.username}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: dataSourceInstance, field: 'password', 'error')} ">
	<label for="password">
		<g:message code="dataSource.password.label" default="Password" />
		
	</label>
	<g:textField name="password" maxlength="20" value="${dataSourceInstance?.password}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: dataSourceInstance, field: 'driver', 'error')} required">
	<label for="driver">
		<g:message code="dataSource.driver.label" default="Driver" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="driver" maxlength="100" required="" value="${dataSourceInstance?.driver}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: dataSourceInstance, field: 'description', 'error')} required">
	<label for="description">
		<g:message code="dataSource.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="description" maxlength="200" required="" value="${dataSourceInstance?.description}"/>
</div>