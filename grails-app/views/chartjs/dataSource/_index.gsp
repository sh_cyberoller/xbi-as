<%@ page import="com.cyberoller.xbi.chartjs.DataSource" %>
<table>
	<thead>
		<tr>
			<g:sortableColumn property="name" title="${message(code: 'dataSource.name.label', default: 'Name')}" />
			<g:sortableColumn property="url" title="${message(code: 'dataSource.url.label', default: 'Url')}" />
			<g:sortableColumn property="username" title="${message(code: 'dataSource.username.label', default: 'Username')}" />
			<g:sortableColumn property="password" title="${message(code: 'dataSource.password.label', default: 'Password')}" />
			<g:sortableColumn property="driver" title="${message(code: 'dataSource.driver.label', default: 'Driver')}" />
			<g:sortableColumn property="description" title="${message(code: 'dataSource.description.label', default: 'Description')}" />
		</tr>
	</thead>
	<tbody>
	<g:each in="${dataSourceInstanceList}" status="i" var="dataSourceInstance">
		<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
			<td><g:link action="show" id="${dataSourceInstance.id}">${fieldValue(bean: dataSourceInstance, field: "name")}</g:link></td>
			<td>${fieldValue(bean: dataSourceInstance, field: "url")}</td>
			<td>${fieldValue(bean: dataSourceInstance, field: "username")}</td>
			<td>${fieldValue(bean: dataSourceInstance, field: "password")}</td>
			<td>${fieldValue(bean: dataSourceInstance, field: "driver")}</td>
			<td>${fieldValue(bean: dataSourceInstance, field: "description")}</td>
		</tr>
	</g:each>
	</tbody>
</table>
<div class="pagination">
	<g:paginate total="${dataSourceInstanceCount ?: 0}" />
</div>
