<%@ page import="com.cyberoller.xbi.chartjs.DataSource" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'dataSource.label', default: 'DataSource')}" />
		<title>
			<g:message code="default.show.label" args="[entityName]" />
			~
			<g:message code="global.app.name" />	
		</title>
		<script type="text/javascript">
			$(document).ready(function() {
				/*监听上传文件 onClick事件*/
				$("#testConnectionBtn").click(function(){
					$.confirm({
						message: "${message(code:'dataSource.testConnection.prompt', default:'Test Connection?')}",
						okText: "${message(code:'custom.button.confirm', default:'Confirm')}",
						cancelText: "${message(code:'custom.button.cancel', default:'Cancel')}",
						css: {
							"background-color": "white",
							"color": "red"
						},
						onOk: function () {
							$.blockUI({fadeIn:0, message : "${message(code:'dataSource.testConnection.loadingMessage', default:'Test Connection ...')}"});
							$.ajax({
								url: "${createLink( action:'testConnection', id:dataSourceInstance?.id)}"
							}).done(function(result) {
								$.alert({message:result, onOk : function(){
									$.unblockUI();
								}});
							});
						},
                        onCancel: function () {
							$.unblockUI();
                        }
					});
				});
			});
		</script>
	</head>
	<body>
		<a href="#show-dataSource" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="show-dataSource" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			
			<g:render template="/chartjs/dataSource/show" />

			<g:form url="[resource:dataSourceInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					
					<g:link class="edit" action="edit" resource="${dataSourceInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					
					<g:actionSubmit class="delete" action="delete" 
						value="${message(code: 'default.button.delete.label', default: 'Delete')}" 
						onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />

					<a href="javascript:void(0)" id="testConnectionBtn" class="edit">
						${message(code:'custom.testConnectionBtn.button', default : 'Test Connection!')}
					</a>
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
