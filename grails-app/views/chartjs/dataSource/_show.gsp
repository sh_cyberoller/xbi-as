<%@ page import="com.cyberoller.xbi.chartjs.DataSource" %>
<ol class="property-list dataSource">

	<g:if test="${dataSourceInstance?.name}">
	<li class="fieldcontain">
		<span id="name-label" class="property-label"><g:message code="dataSource.name.label" default="Name" /></span>
		<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${dataSourceInstance}" field="name"/></span>
	</li>
	</g:if>

	<g:if test="${dataSourceInstance?.url}">
	<li class="fieldcontain">
		<span id="url-label" class="property-label"><g:message code="dataSource.url.label" default="Url" /></span>
		<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${dataSourceInstance}" field="url"/></span>
	</li>
	</g:if>

	<g:if test="${dataSourceInstance?.username}">
	<li class="fieldcontain">
		<span id="username-label" class="property-label"><g:message code="dataSource.username.label" default="Username" /></span>
		<span class="property-value" aria-labelledby="username-label"><g:fieldValue bean="${dataSourceInstance}" field="username"/></span>
	</li>
	</g:if>

	<g:if test="${dataSourceInstance?.password}">
	<li class="fieldcontain">
		<span id="password-label" class="property-label"><g:message code="dataSource.password.label" default="Password" /></span>
		<span class="property-value" aria-labelledby="password-label"><g:fieldValue bean="${dataSourceInstance}" field="password"/></span>
	</li>
	</g:if>

	<g:if test="${dataSourceInstance?.driver}">
	<li class="fieldcontain">
		<span id="driver-label" class="property-label"><g:message code="dataSource.driver.label" default="Driver" /></span>
		<span class="property-value" aria-labelledby="driver-label"><g:fieldValue bean="${dataSourceInstance}" field="driver"/></span>
	</li>
	</g:if>

	<g:if test="${dataSourceInstance?.description}">
	<li class="fieldcontain">
		<span id="description-label" class="property-label"><g:message code="dataSource.description.label" default="Description" /></span>
		<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${dataSourceInstance}" field="description"/></span>
	</li>
	</g:if>

	<g:if test="${dataSourceInstance?.owner}">
	<li class="fieldcontain">
		<span id="owner-label" class="property-label"><g:message code="dataSource.owner.label" default="Owner" /></span>
		<span class="property-value" aria-labelledby="owner-label">
			<g:link controller="user" action="show" id="${dataSourceInstance?.owner?.id}">${dataSourceInstance?.owner?.encodeAsHTML()}</g:link>
		</span>
	</li>
	</g:if>

	<g:if test="${dataSourceInstance?.createdBy}">
	<li class="fieldcontain">
		<span id="createdBy-label" class="property-label"><g:message code="dataSource.createdBy.label" default="Created By" /></span>
		<span class="property-value" aria-labelledby="createdBy-label">
			<g:link controller="user" action="show" id="${dataSourceInstance?.createdBy?.id}">${dataSourceInstance?.createdBy?.encodeAsHTML()}</g:link>
		</span>
	</li>
	</g:if>

	<g:if test="${dataSourceInstance?.lastModifiedBy}">
	<li class="fieldcontain">
		<span id="lastModifiedBy-label" class="property-label"><g:message code="dataSource.lastModifiedBy.label" default="Last Modified By" /></span>
		<span class="property-value" aria-labelledby="lastModifiedBy-label">
			<g:link controller="user" action="show" id="${dataSourceInstance?.lastModifiedBy?.id}">${dataSourceInstance?.lastModifiedBy?.encodeAsHTML()}</g:link>
		</span>
	</li>
	</g:if>

	<g:if test="${dataSourceInstance?.dateCreated}">
	<li class="fieldcontain">
		<span id="dateCreated-label" class="property-label"><g:message code="dataSource.dateCreated.label" default="Date Created" /></span>
		<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${dataSourceInstance?.dateCreated}" /></span>
	</li>
	</g:if>

	<g:if test="${dataSourceInstance?.lastUpdated}">
	<li class="fieldcontain">
		<span id="lastUpdated-label" class="property-label"><g:message code="dataSource.lastUpdated.label" default="Last Updated" /></span>
		<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${dataSourceInstance?.lastUpdated}" /></span>
	</li>
	</g:if>

</ol>