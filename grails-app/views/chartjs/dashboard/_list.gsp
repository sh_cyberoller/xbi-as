<%@ page import="com.cyberoller.xbi.chartjs.Dashboard" %>
<table>
	<thead>
		<tr>
			<g:sortableColumn property="name" title="${message(code: 'dashboard.name.label', default: 'Name')}" />
			<g:sortableColumn property="author" title="${message(code: 'dashboard.author.label', default: 'Author')}" />
			<g:sortableColumn property="title" title="${message(code: 'dashboard.title.label', default: 'Title')}" />
			<g:sortableColumn property="description" title="${message(code: 'dashboard.description.label', default: 'Description')}" />
			<g:sortableColumn property="isActive" title="${message(code: 'dashboard.isActive.label', default: 'Is Active')}" />
			<g:sortableColumn property="dateCreated" title="${message(code: 'dashboard.dateCreated.label', default: 'Date Created')}" />
			<g:sortableColumn property="lastUpdated" title="${message(code: 'dashboard.lastUpdated.label', default: 'Last Updated')}" />
			<th><g:message code="dashboard.button.run.label" default="RUN" /></th>
		</tr>
	</thead>
	<tbody>
		<g:each in="${dashboardInstanceList}" status="i" var="dashboardInstance">
		<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
			<td>
				<g:link action="show" id="${dashboardInstance.id}">${fieldValue(bean: dashboardInstance, field: "name")}</g:link>
			</td>
			<td>${fieldValue(bean: dashboardInstance, field: "author")}</td>
			<td>${fieldValue(bean: dashboardInstance, field: "title")}</td>
			<td>${fieldValue(bean: dashboardInstance, field: "description")}</td>
			<td><g:formatBoolean boolean="${dashboardInstance.isActive}" /></td>
			<td><g:formatDate date="${dashboardInstance.dateCreated}" /></td>
			<td><g:formatDate date="${dashboardInstance.lastUpdated}" /></td>
			<td>
				<g:link action="run" id="${dashboardInstance?.id}" params="[action:'list']">
					<g:message code="dashboard.button.run.label" default="RUN" />
				</g:link>
			</td>
		</tr>
		</g:each>
	</tbody>
</table>
<div class="pagination">
	<g:paginate total="${dashboardInstanceCount ?: 0}" />
</div>