<%@ page import="com.cyberoller.xbi.chartjs.Dashboard" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'dashboard.label', default: 'Dashboard')}" />

		<title>
			<g:message code="dashboard.label" />
			-
			<g:message code="dashboard.button.run.label" />
			~
			<g:message code="global.app.name" default="XBI-AS" />
		</title>
		
		<script type="text/javascript" src="${resource(dir: 'js', file: 'globalize.min.js')}" ></script>
		<script type="text/javascript" src="${resource(dir: 'js', file: 'dx.chartjs.js')}" ></script>
		<script type="text/javascript" src="${resource(dir: 'js', file: 'jquery-ui-numeric-min.js')}" ></script>

		<script type="text/javascript">
			var baseUrl = "${createLink( controller:'dashboard', action:'data', params:[id : dashboardInstance?.id])}";
			$(document).ready(function(){
				$.ajax({
					url: "${createLink( controller:'dashboard', action:'template', params:[id : dashboardInstance?.id])}"
				}).done(function(html){
					$("#dashboard_template").html(html)
					${dashboardInstance?.name}();
				});
			});
		</script>
	</head>
	<body>

		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>

		<div id="dashboard_template">
			
		</div>

	</body>
</html>
