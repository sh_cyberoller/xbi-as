<%@ page import="com.cyberoller.xbi.chartjs.Dashboard" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'dashboard.label', default: 'Dashboard')}" />
		<title>
			<g:message code="default.show.label" args="[entityName]" />
			~
			<g:message code="global.app.name" default="XBI-AS" />
		</title>

		<script type="text/javascript">
			function runDashboard(id){
				$.confirm({
					message: "打开仪表盘？",
					okText: "确定",
					cancelText: "取消",
					css: {
						"background-color": "white",
						"color": "red"
					},
					onOk: function () {
						$.blockUI({fadeIn:0, message : "加载仪表盘..."});
						var href = "${createLink(controller:'dashboard', action:'run', id:'')}"
						window.location.href = href + id;
					},
					onCancel: function () {
						$.unblockUI();
					}
				});
			}
			$(document).ready(function() {
				/*监听上传文件 onClick事件*/
				$("#uploadXFileBtn").click(function(){
					$.confirm({
						message: "${message(code:'xfile.upload.confirm.message', default:'Upload?')}",
						okText: "${message(code:'custom.button.confirm', default:'Confirm')}",
						cancelText: "${message(code:'custom.button.cancel', default:'Cancel')}",
						css: {
							"background-color": "white",
							"color": "red"
						},
						onOk: function () {
							$.blockUI({fadeIn:0, message : "开始上文件..."});
							$("#uploadXFile").submit();
						},
                        onCancel: function () {
							$.unblockUI();
                        }
					});
				});
			});
		</script>

	</head>
	<body>
		
		<a href="#show-dashboard" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="show-dashboard" class="content scaffold-show" role="main">
			
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			
			<g:render template="/chartjs/dashboard/show" />			

			<fieldset class="buttons">

				<g:link class="edit" action="edit" resource="${dashboardInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>

				<g:actionSubmit class="delete" action="delete" 
					value="${message(code: 'default.button.delete.label', default: 'Delete')}" 
					onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />

				<a href="javascript:void(0)" id="uploadXFileBtn" class="edit">
					${message(code:'custom.upload.button', default : 'Upload!')}
				</a>
				
				
				<g:link action="run" id="${dashboardInstance?.id}" params="[action:'show']" class="edit">
					<g:message code="dashboard.button.run.label" default="RUN" />
				</g:link>
				
				
			</fieldset>
		</div>
	</body>
</html>