<%@ page import="com.cyberoller.xbi.chartjs.Dashboard" %>

<ol class="property-list dashboard">

	<g:if test="${dashboardInstance?.name}">
	<li class="fieldcontain">
		<span id="name-label" class="property-label"><g:message code="dashboard.name.label" default="Name" /></span>
		<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${dashboardInstance}" field="name"/></span>
	</li>
	</g:if>

	<g:if test="${dashboardInstance?.author}">
	<li class="fieldcontain">
		<span id="author-label" class="property-label"><g:message code="dashboard.author.label" default="Author" /></span>
		<span class="property-value" aria-labelledby="author-label"><g:fieldValue bean="${dashboardInstance}" field="author"/></span>
	</li>
	</g:if>

	<g:if test="${dashboardInstance?.title}">
	<li class="fieldcontain">
		<span id="title-label" class="property-label"><g:message code="dashboard.title.label" default="Title" /></span>
		<span class="property-value" aria-labelledby="title-label"><g:fieldValue bean="${dashboardInstance}" field="title"/></span>
	</li>
	</g:if>

	<g:if test="${dashboardInstance?.description}">
	<li class="fieldcontain">
		<span id="description-label" class="property-label"><g:message code="dashboard.description.label" default="Description" /></span>
		<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${dashboardInstance}" field="description"/></span>
	</li>
	</g:if>

	<g:if test="${dashboardInstance?.atMenu}">
	<li class="fieldcontain">
		<span id="atMenu-label" class="property-label"><g:message code="dashboard.atMenu.label" default="At Menu?" /></span>
		<span class="property-value" aria-labelledby="atMenu-label"><g:formatBoolean boolean="${dashboardInstance?.atMenu}" /></span>
	</li>
	</g:if>
	
	<g:if test="${dashboardInstance?.menuOrder}">
	<li class="fieldcontain">
		<span id="menuOrder-label" class="property-label"><g:message code="dashboard.menuOrder.label" default="Menu Order" /></span>
		<span class="property-value" aria-labelledby="menuOrder-label"><g:fieldValue bean="${dashboardInstance}" field="menuOrder"/></span>
	</li>
	</g:if>	
	
	<g:if test="${dashboardInstance?.homePage}">
	<li class="fieldcontain">
		<span id="homePage-label" class="property-label"><g:message code="dashboard.homePage.label" default="Home Page?" /></span>
		<span class="property-value" aria-labelledby="homePage-label"><g:formatBoolean boolean="${dashboardInstance?.homePage}" /></span>
	</li>
	</g:if>

	<g:if test="${dashboardInstance?.isActive}">
	<li class="fieldcontain">
		<span id="isActive-label" class="property-label"><g:message code="dashboard.isActive.label" default="Is Active" /></span>
		<span class="property-value" aria-labelledby="isActive-label"><g:formatBoolean boolean="${dashboardInstance?.isActive}" /></span>
	</li>
	</g:if>
	
	<g:if test="${dashboardInstance?.owner}">
	<li class="fieldcontain">
		<span id="owner-label" class="property-label"><g:message code="dashboard.owner.label" default="Owner" /></span>
		<span class="property-value" aria-labelledby="owner-label">
			<g:link controller="user" action="show" id="${dashboardInstance?.owner?.id}">${dashboardInstance?.owner?.encodeAsHTML()}</g:link>
		</span>
	</li>
	</g:if>

	<g:if test="${dashboardInstance?.createdBy}">
	<li class="fieldcontain">
		<span id="createdBy-label" class="property-label"><g:message code="dashboard.createdBy.label" default="Created By" /></span>
		<span class="property-value" aria-labelledby="createdBy-label">
			<g:link controller="user" action="show" id="${dashboardInstance?.createdBy?.id}">${dashboardInstance?.createdBy?.encodeAsHTML()}</g:link>
		</span>
	</li>
	</g:if>

	<g:if test="${dashboardInstance?.lastModifiedBy}">
	<li class="fieldcontain">
		<span id="lastModifiedBy-label" class="property-label"><g:message code="dashboard.lastModifiedBy.label" default="Last Modified By" /></span>
		<span class="property-value" aria-labelledby="lastModifiedBy-label">
			<g:link controller="user" action="show" id="${dashboardInstance?.lastModifiedBy?.id}">${dashboardInstance?.lastModifiedBy?.encodeAsHTML()}</g:link>
		</span>
	</li>
	</g:if>
	

	<g:if test="${dashboardInstance?.dateCreated}">
	<li class="fieldcontain">
		<span id="dateCreated-label" class="property-label"><g:message code="dashboard.dateCreated.label" default="Date Created" /></span>
		<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${dashboardInstance?.dateCreated}" /></span>
	</li>
	</g:if>

	<g:if test="${dashboardInstance?.lastUpdated}">
	<li class="fieldcontain">
		<span id="lastUpdated-label" class="property-label"><g:message code="dashboard.lastUpdated.label" default="Last Updated" /></span>
		<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${dashboardInstance?.lastUpdated}" /></span>
	</li>
	</g:if>

</ol>

<table>
	<caption><g:message code="custom.upload.button" default="Upload" /></caption>
	<thead>
		<tr>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				<g:uploadForm name="uploadXFile" action="upload">
					<g:hiddenField name="id" value="${dashboardInstance?.id}"/>
					<input type="file" name="xfile" id="xfile" class="upload-file-input-file" />
				</g:uploadForm>
			</td>
		</tr>
	</tbody>
</table>