<div class="phone-switcher">
	<div class="selected" data-show="#day">
		<div class="left"><span>日销售</span></div>
	</div>
	<div data-show="#month">
		<div class="right"><span>月销售</span></div>
	</div>  
</div>
<div class="layout-box lb-1" id="day">
	<div class="hb-1">
		<div class="block-header clear-fix">
			<h3>日销售</h3>
			<div class="hr"></div>
		</div>
		<div class="perf-items clear-fix">
			<div class="perf-item layout-box lb-2">
				今天
				<div id="dTodaySales"></div>
			</div>
			<div class="perf-item layout-box lb-2">
				昨天
				<div id="dYesterdaySales"></div>
			</div>
			<div class="perf-item layout-box lb-2">
				上周
				<div id="dLastWeekSales"></div>
			</div>
			<div class="perf-item layout-box lb-2 tablet-hide">
				去年
				<div class="annualPerfLastYearSales"></div>
			</div>
		</div>
		<div class="perf-graph">
			<div id="dailySalesChart"></div>
		</div>
		<div>
			<table class="perf-graph-navigation phone-hide">
				<tr>
					<td class="prev"><div id="getPrevDaySales"><span></span></div></td>
					<td class="yesterday"><div id="getLastDaySales">昨天</div></td>
					<td class="today"><div id="getThisDaySales" class="active">今天</div></td>
					<td class="next"><div id="getNextDaySales" class="disabled"><span></span></div></td>
				</tr>
			</table>
		</div>
	</div>   

	<div class="hb-2 phone-hide">
		<div class="block-header clear-fix">
			<h3>年销售</h3>
			<div class="hr"></div>
		</div>
		<div class="perf-gauges clear-fix">
			<div class="layout-box lb-3">
				<div class="perf-gauge" id="perfGaugeYTDSales"></div>
				<div class="perf-item">
					自然年
					<div id="annualPerfYTDSales"></div>
				</div>
			</div>
			<div class="layout-box lb-3">
				<div class="perf-gauge" id="perfGaugeLastYearSales"></div>
				<div class="perf-item">
					财务年<span id="fiscalYear"></span>
					<div class="annualPerfLastYearSales"></div>
				</div>
			</div>
		</div>
	</div> 
	
	<div class="hb-3 phone-hide">
		<div class="header-surround">
			<div class="block-header clear-fix">
				<h3>销售预测 <span id="currentYear"></span></h3>
				<div class="hr"></div>
			</div>
		</div>
		<div class="forecast-graph" id="forecastGraph"></div>
	</div>
</div>

<div class="layout-box lb-1 phone-hide" id="month">
	<div class="hb-1">
		<div class="block-header clear-fix">
			<h3>月销售</h3>
			<div class="hr"></div>
		</div>
		<div class="perf-items clear-fix">
			<div class="perf-item layout-box lb-2">
				本月
				<div id="mThisMonthSales"></div>
			</div>
			<div class="perf-item layout-box lb-2  right">
				上月
				<div id="mLastMonthSales"></div>
			</div>
			<div class="perf-item layout-box lb-2">
				YTD
				<div id="mYTDSales"></div>
			</div>
			<div class="perf-item layout-box lb-2 right tablet-hide">
				去年销售额
				<div class="annualPerfLastYearSales"></div>
			</div>
		</div>
		<div class="perf-graph">
			<div id="monthlySalesChart"></div>
		</div>
		<div>
			<table class="perf-graph-navigation phone-hide">
				<tr>
					<td class="prev"><div id="getPrevMonthSales"><span></span></div></td>
					<td class="yesterday"><div id="getLastMonthSales">上月</div></td>
					<td class="today"><div class="active" id="getThisMonthSales">本月</div></td>
					<td class="next"><div class="disabled" id="getNextMonthSales"><span></span></div></td>
				</tr>
			</table>
		</div>
	</div>

	<div class="hb-4 phone-hide">
		<div class="block-header clear-fix">
			<h3>销售部门</h3>
			<div class="hr"></div>
		</div>
		<div class="pie-graph" id="pieChart"></div>
	</div>
</div>