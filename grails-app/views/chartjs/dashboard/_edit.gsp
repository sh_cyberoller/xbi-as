<%@ page import="com.cyberoller.xbi.chartjs.Dashboard" %>

<div class="fieldcontain ${hasErrors(bean: dashboardInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="dashboard.name.label" default="Name" />
	</label>
	<g:textField name="name" maxlength="20" value="${dashboardInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: dashboardInstance, field: 'author', 'error')} ">
	<label for="author">
		<g:message code="dashboard.author.label" default="Author" />
	</label>
	<g:textField name="author" maxlength="20" value="${dashboardInstance?.author}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: dashboardInstance, field: 'title', 'error')} ">
	<label for="title">
		<g:message code="dashboard.title.label" default="Title" />
	</label>
	<g:textField name="title" maxlength="200" value="${dashboardInstance?.title}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: dashboardInstance, field: 'datasource', 'error')} required">
	<label for="datasource">
		<g:message code="dashboard.datasource.label" default="Datasource" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="datasource" name="datasource.id" from="${com.cyberoller.xbi.chartjs.DataSource.list()}" optionKey="id" required="" value="${dashboardInstance?.datasource?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: dashboardInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="dashboard.description.label" default="Description" />
	</label>
	<g:textField name="description" maxlength="200" value="${dashboardInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: dashboardInstance, field: 'atMenu', 'error')} ">
	<label for="atMenu">
		<g:message code="dashboard.atMenu.label" default="At Menu?" />
	</label>
	<g:checkBox name="atMenu" value="${dashboardInstance?.atMenu}" />
</div>

<div class="fieldcontain ${hasErrors(bean: dashboardInstance, field: 'menuOrder', 'error')} ">
	<label for="menuOrder">
		<g:message code="dashboard.menuOrder.label" default="Menu Order" />
	</label>
	<g:textField name="menuOrder" maxlength="2" value="${dashboardInstance?.menuOrder}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: dashboardInstance, field: 'homePage', 'error')} ">
	<label for="homePage">
		<g:message code="dashboard.homePage.label" default="Home Page?" />
	</label>
	<g:checkBox name="homePage" value="${dashboardInstance?.homePage}" />
</div>

<div class="fieldcontain ${hasErrors(bean: dashboardInstance, field: 'isActive', 'error')} ">
	<label for="isActive">
		<g:message code="dashboard.isActive.label" default="Is Active" />
	</label>
	<g:checkBox name="isActive" value="${dashboardInstance?.isActive}" />
</div>