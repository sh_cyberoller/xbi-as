<%@ page import="com.cyberoller.xbi.chartjs.Dashboard" %>
<!DOCTYPE html>
<html>
	<head>

		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'dashboard.label', default: 'Dashboard')}" />

		<title>
			<g:message code="default.list.label" args="[entityName]" />
			~
			<g:message code="global.app.name" default="XBI-AS" />
		</title>
	</head>
	<body>
		<a href="#list-dashboard" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-dashboard" class="content scaffold-list" role="main">

			<h1>
				<g:message code="default.list.label" args="[entityName]" />
				<g:link action="create" class="edit">(+)</g:link>
			</h1>

			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			
			<g:render template="/chartjs/dashboard/list" />

		</div>
	</body>
</html>
