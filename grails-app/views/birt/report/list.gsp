<%@ page import="com.cyberoller.xbi.birt.Report" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">

		<g:set var="entityName" value="${message(code: 'report.label', default: 'Report')}" />

		<title>
			<g:message code="default.list.label" args="[entityName]" />
			~
			<g:message code="global.app.name" default="XBI" />
		</title>
		<script type="text/javascript">
			function runBirtReport(id){
				/*直接运行，不再提示是否运行*/
				var href = "${createLink(controller:'birt', action:'run', id:'')}"
				window.location.href = href + id;
				/*
				$.confirm({
					message: "运行报表？",
					okText: "确定",
					cancelText: "取消",
					css: {
						"background-color": "white",
						"color": "red"
					},
					onOk: function () {
						$.blockUI({fadeIn:0, message : "运行报表..."});
						var href = "${createLink(controller:'birt', action:'run', id:'')}"
						window.location.href = href + id;
					},
					onCancel: function () {
						$.unblockUI();
					}
				});
				*/
				/*
				jqConfirm("提示", "运行报表？", function(){
					$.blockUI({fadeIn:0, message : "运行报表..."});
					var href = "${createLink(controller:'birt', action:'run', id:'')}"
					window.location.href = href + "/" + id;
				}, function(){
					//do nothing				
				})
				*/
			}
		</script>
	</head>
	<body>
		<a href="#list-report" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-report" class="content scaffold-list" role="main">

			<h1>
				<g:message code="default.list.label" args="[entityName]" />
				<g:link action="create" class="edit">(+)</g:link>
			</h1>

			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>

			<g:render template="/birt/report/list" />

			<div class="pagination">
				<g:paginate total="${reportInstanceTotal}" />
			</div>
		</div>
	</body>
</html>