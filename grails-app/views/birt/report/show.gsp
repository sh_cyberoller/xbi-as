<%@ page import="com.cyberoller.xbi.birt.Report" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">

		<g:set var="entityName" value="${message(code: 'report.label', default: 'Report')}" />

		<title>
			<g:message code="default.show.label" args="[entityName]" />
			~
			<g:message code="global.app.name" default="XBI" />
		</title>
		<script type="text/javascript">
			function runBirtReport(id){
				var href = "${createLink(controller:'birt', action:'run', id:'')}"
				window.location.href = href + id;
			}
		</script>
	</head>
	<body>
		<a href="#show-report" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="show-report" class="content scaffold-show" role="main">

			<h1><g:message code="default.show.label" args="[entityName]" /></h1>

			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>

			<g:render template="/birt/report/show" />

			<g:if test="${reportInstance?.reportParameters}">
				<g:render template="/birt/report/parameter/list" />
			</g:if>

			<fieldset class="buttons">
				<a class="edit" href="javascript:void(0)" onclick="runBirtReport('${reportInstance.name}')">
					<g:message code="birt.view.run" default="RUN" />
				</a>
			</fieldset>
		</div>
	</body>
</html>