<ol class="property-list report">

	<g:if test="${reportInstance?.name}">
	<li class="fieldcontain">
		<span id="name-label" class="property-label">
			<g:message code="report.name.label" default="Name" />
		</span>
		
		<span class="property-value" aria-labelledby="name-label">
			<g:fieldValue bean="${reportInstance}" field="name"/>
		</span>
	</li>
	</g:if>

	<g:if test="${reportInstance?.author}">
	<li class="fieldcontain">
		<span id="author-label" class="property-label">
			<g:message code="report.author.label" default="Author" />
		</span>
		
		<span class="property-value" aria-labelledby="author-label">
			<g:fieldValue bean="${reportInstance}" field="author"/>
		</span>
	</li>
	</g:if>

	<g:if test="${reportInstance?.title}">
	<li class="fieldcontain">
		<span id="title-label" class="property-label">
			<g:message code="report.title.label" default="Title" />
		</span>
		
		<span class="property-value" aria-labelledby="title-label">
			<g:fieldValue bean="${reportInstance}" field="title"/>
		</span>
	</li>
	</g:if>

	<g:if test="${reportInstance?.createdBy}">
	<li class="fieldcontain">
		<span id="createdBy-label" class="property-label">
			<g:message code="report.createdBy.label" default="Created By" />
		</span>
		
		<span class="property-value" aria-labelledby="createdBy-label">
			<g:fieldValue bean="${reportInstance}" field="createdBy"/>
		</span>
	</li>
	</g:if>

	<g:if test="${reportInstance?.description}">
	<li class="fieldcontain">
		<span id="description-label" class="property-label">
			<g:message code="report.description.label" default="Description" />
		</span>
		
		<span class="property-value" aria-labelledby="description-label">
			<g:fieldValue bean="${reportInstance}" field="description"/>
		</span>
	</li>
	</g:if>

	<g:if test="${reportInstance?.comment}">
	<li class="fieldcontain">
		<span id="comment-label" class="property-label">
			<g:message code="report.comment.label" default="Comment" />
		</span>
		
		<span class="property-value" aria-labelledby="comment-label">
			<g:fieldValue bean="${reportInstance}" field="comment"/>
		</span>
	</li>
	</g:if>

	<g:if test="${reportInstance?.units}">
	<li class="fieldcontain">
		<span id="units-label" class="property-label">
			<g:message code="report.units.label" default="Units" />
		</span>
		
		<span class="property-value" aria-labelledby="units-label">
			<g:fieldValue bean="${reportInstance}" field="units"/>
		</span>
	</li>
	</g:if>

	<g:if test="${reportInstance?.helpGuide}">
	<li class="fieldcontain">
		<span id="helpGuide-label" class="property-label">
			<g:message code="report.helpGuide.label" default="Help Guide" />
		</span>
		
		<span class="property-value" aria-labelledby="helpGuide-label">
			<g:fieldValue bean="${reportInstance}" field="helpGuide"/>
		</span>
	</li>
	</g:if>

	<g:if test="${reportInstance?.filename}">
	<li class="fieldcontain">
		<span id="filename-label" class="property-label">
			<g:message code="report.filename.label" default="FileName" />
		</span>
		
		<span class="property-value" aria-labelledby="filename-label">
			<g:fieldValue bean="${reportInstance}" field="filename"/>
		</span>
	</li>
	</g:if>

	<g:if test="${reportInstance?.fullfile}">
	<li class="fieldcontain">
		<span id="fullfile-label" class="property-label">
			<g:message code="report.fullfile.label" default="Fullfile" />
		</span>
		
		<span class="property-value" aria-labelledby="fullfile-label">
			<g:fieldValue bean="${reportInstance}" field="fullfile"/>
		</span>
		
	</li>
	</g:if>

	<g:if test="${reportInstance?.dateCreated}">
	<li class="fieldcontain">
		<span id="dateCreated-label" class="property-label">
			<g:message code="report.dateCreated.label" default="Date Created" />
		</span>
		
		<span class="property-value" aria-labelledby="dateCreated-label">
			<g:formatDate date="${reportInstance?.dateCreated}" />
		</span>
	</li>
	</g:if>

	<g:if test="${reportInstance?.lastUpdated}">
	<li class="fieldcontain">
		<span id="lastUpdated-label" class="property-label">
			<g:message code="report.lastUpdated.label" default="Last Updated" />
		</span>
		
		<span class="property-value" aria-labelledby="lastUpdated-label">
			<g:formatDate date="${reportInstance?.lastUpdated}" />
		</span>
	</li>
	</g:if>

</ol>