<table>
	<thead>
		<tr>
			<g:sortableColumn property="name" title="${message(code: 'report.name.label', default: 'Name')}" />
			<g:sortableColumn property="title" title="${message(code: 'report.title.label', default: 'Title')}" />
			<g:sortableColumn property="description" title="${message(code: 'report.description.label', default: 'Description')}" />
			<g:sortableColumn property="comment" title="${message(code: 'report.comment.label', default: 'Comment')}" />
			<th><g:message code="birt.view.run" default="RUN" /></th>
		</tr>
	</thead>
	<tbody>
	<g:each in="${reportInstanceList}" status="i" var="reportInstance">
		<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
			<td>
				<g:link action="show" id="${reportInstance.id}">${fieldValue(bean: reportInstance, field: "name")}</g:link>
			</td>
			<td>${fieldValue(bean: reportInstance, field: "title")}</td>
			<td>${fieldValue(bean: reportInstance, field: "description")}</td>
			<td>${fieldValue(bean: reportInstance, field: "comment")}</td>
			<td>
				<a href="javascript:void(0)" onclick="runBirtReport('${reportInstance.name}')">
					<g:message code="birt.view.run" default="RUN" />
				</a>
			</td>
		</tr>
	</g:each>
	</tbody>
</table>