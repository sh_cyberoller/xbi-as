<%@ page import="com.cyberoller.xbi.birt.ReportParameter" %>
<table>
	<caption>
		<g:message code="report.reportParameters.label" default="Parameters" />
	</caption>
	<thead>
		<tr>
			<th><g:message code="reportParameter.name.label" default="Name" /></th>
			<th><g:message code="reportParameter.datatype.label" default="Datatype" /></th>
			<th><g:message code="reportParameter.prompttext.label" default="Prompttext" /></th>
			<th><g:message code="reportParameter.allowblank.label" default="Allowblank" /></th>
			<th><g:message code="reportParameter.hidden.label" default="Hidden" /></th>
			<th><g:message code="reportParameter.dateCreated.label" default="Date Created" /></th>
			<th><g:message code="reportParameter.lastUpdated.label" default="Last Updated" /></th>
		</tr>
	</thead>
	<tbody>
	<g:each in="${reportInstance?.reportParameters?.sort{return it?.id}}" status="i" var="reportParameterInstance">
		<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
			<td>
				${fieldValue(bean: reportParameterInstance, field: "name")}
			</td>
			<td>
				<g:message code="reportParameter.datatype.value.${reportParameterInstance?.datatype}" default="${reportParameterInstance?.datatype}"/>
			</td>
			<td>
				${fieldValue(bean: reportParameterInstance, field: "prompttext")}
			</td>
			<td>
				<g:formatBoolean boolean="${reportParameterInstance.allowblank}" />
			</td>
			<td>
				<g:formatBoolean boolean="${reportParameterInstance.hidden}" />
			</td>
			<td>
				<g:formatDate date="${reportParameterInstance.dateCreated}" />
			</td>
			<td>
				<g:formatDate date="${reportParameterInstance.lastUpdated}" />
			</td>
		</tr>
	</g:each>
	</tbody>
</table>