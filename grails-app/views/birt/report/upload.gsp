<%@ page import="com.cyberoller.xbi.birt.Report" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">

		<g:set var="entityName" value="${message(code: 'report.label', default: 'Report')}" />

		<title>
			<g:message code="default.show.label" args="[entityName]" />
			~
			<g:message code="global.app.name" default="XBI" />
		</title>
		<script type="text/javascript">
			$(document).ready(function() {
				/*监听上传文件 onClick事件*/
				$("#uploadXFileBtn").click(function(){
					$.confirm({
						message: "${message(code:'xfile.upload.confirm.message', default:'Upload?')}",
						okText: "${message(code:'custom.button.confirm', default:'Confirm')}",
						cancelText: "${message(code:'custom.button.cancel', default:'Cancel')}",
						css: {
							"background-color": "white",
							"color": "red"
						},
						onOk: function () {
							$.blockUI({fadeIn:0, message : "开始上文件..."});
							$("#uploadXFile").submit();
						},
                        onCancel: function () {
							$.unblockUI();
                        }
					});
				});
			});
		</script>
	</head>
	<body>
		<a href="#show-report" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="show-report" class="content scaffold-show" role="main">

			<h1><g:message code="birt.upload.label" default="Upload" /></h1>

			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>

			<table>
				<caption><g:message code="custom.upload.button" default="Upload" /></caption>
				<thead>
					<tr>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<g:uploadForm name="uploadXFile" action="upload">
								<input type="file" name="xfile" id="xfile" class="upload-file-input-file" />
							</g:uploadForm>
						</td>
					</tr>
				</tbody>
			</table>

			

			<fieldset class="buttons">
				<a href="javascript:void(0)" id="uploadXFileBtn" class="edit">
					${message(code:'custom.upload.button', default : 'Upload!')}
				</a>
			</fieldset>
		</div>
	</body>
</html>