<g:if test="${fileInstanceList}">
	<g:each in="${fileInstanceList}" status="i" var="fileInstance">
	<li>
		<g:if test="${fileInstance.xtype == 'Report'}">
			<i class="fa fa-double-angle-right"></i>
			<g:link controller="birt" action="run" id="${fileInstance.report}" data-has-menu="true">
				${fileInstance.reportTitle}(R)
			</g:link>
		</g:if>
		<g:if test="${fileInstance.xtype == 'Dashboard'}">
			<i class="fa fa-double-angle-right"></i>

			<a href="javascript:void(0);" onclick="return loadDashboard(${fileInstance.dashboard});" data-has-menu="true">
				${fileInstance.dashboardTitle}(D)
			</a>
		</g:if>
	</li>
	</g:each>
</g:if>