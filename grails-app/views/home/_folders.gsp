<g:if test="${folderInstanceList}">
	<g:each in="${folderInstanceList}" status="i" var="folderInstance">
	<li>
		<a href="javascript:void(0);" class="dropdown-toggle">
			<i class="fa ${folderInstance?.icon}"></i>
			<span class="menu-text-in">${folderInstance?.name}</span>
			<b class="arrow fa fa-angle-down"></b>
		</a>
		<ul class="submenu">
			<g:include controller="home" action="files" id="${folderInstance?.id}" />
		</ul>
	</li>
	</g:each>
</g:if>