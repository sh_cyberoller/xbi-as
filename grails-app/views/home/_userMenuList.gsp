<g:if test="${dashboardInstanceList}">
	<g:each in="${dashboardInstanceList}" status="i" var="dashboardInstance">     
		<li ><a href="${createLink(controller:'open', action:'dashboard', id:dashboardInstance.id)}">${dashboardInstance.title}</a></li>
	</g:each>
</g:if>