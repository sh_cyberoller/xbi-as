<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><g:layoutTitle default="XBI"/></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
	<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
	<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">

	<script type="text/javascript" src="${resource(dir: 'dashboard/as001/js', file: 'jquery-1.11.0.min.js')}" ></script>

	<script type="text/javascript" src="${resource(dir: 'dashboard/as001/js', file: 'globalize.min.js')}" ></script>
	<script type="text/javascript" src="${resource(dir: 'dashboard/as001/js', file: 'dx.chartjs.js')}" ></script>

	<script type="text/javascript" src="${resource(dir: 'dashboard/as001/js', file: 'my-numeric.js')}" ></script>

	<link rel="stylesheet" type="text/css" href="${resource(dir: 'dashboard/as001/css', file: 'reset.css')}" />
	<link rel="stylesheet" type="text/css" href="${resource(dir: 'dashboard/as001/css', file: 'site.css')}" media="screen and (min-width: 740px)" />
	<link rel="stylesheet" type="text/css" href="${resource(dir: 'dashboard/as001/css', file: 'phone.css')}" media="screen and (max-width: 739px)"/>

	<script type="text/javascript" src="${resource(dir: 'dashboard/as001/js', file: 'salesDashboard.js')}" ></script>
	<script type="text/javascript" src="${resource(dir: 'dashboard/as001/js', file: 'themes.js')}" ></script>

	<g:layoutHead/>
	<g:javascript library="application"/>		
	<r:layoutResources />
</head>
<body>
	<header class="dashboard-header">
		<div class="supervisor-info">
			<div class="avatar"></div>
			<div class="name">XBI</div>
		</div>
		<div class="dashboard-title">
			<div class="date" id="currentDate"></div>
			<h1>仪表盘 - 销售和收入（傲胜）</h1>
		</div>
	</header>
	<section class="dashboard-navigation">
		<ul>
			<li>
				<a href="Index">
					<div id="index" class="navigation-item clear-fix">
						<div class="icon dashboard"></div>
						<div>
							<h2>销<span>售</span></h2>
							<span>收入快照</span>
						</div>
					</div>
				</a>
			</li>
			<li>
				<a href="Products">
					<div id="products" class="navigation-item clear-fix">
						<div class="icon products"></div>
						<div>
							<h2>产品</h2>
							<span>产品收入</span>
						</div>
					</div>
				</a>
			</li>
			<li class="phone-hide">
				<a href="sectors">
					<div id="sectors" class="navigation-item clear-fix">
						<div class="icon sectors"></div>
						<div>
							<h2>部门</h2>
							<span>部门收入</span>
						</div>
					</div>
				</a>
			</li>
			<li>
				<a href="regions">
					<div id="regions" class="navigation-item clear-fix">
						<div class="icon regions"></div>
						<div>
							<h2>区域</h2>
							<span>区域收入</span>
						</div>
					</div>
				</a>
			</li>
			<li>
				<a href="channels">
					<div id="channels" class="navigation-item clear-fix">
						<div class="icon channels"></div>
						<div>
							<h2>渠道</h2>
							<span>渠道收入</span>
						</div>
					</div>
				</a>
			</li>
		</ul>
		<div class="footer-logo"></div>
	</section>

	<header class="dashboard-mobile-header tablet-hide">
		<h1>Sales Dashboard</h1>
	</header>

	<section class="main-content">
		<div id="dashboard-content" class="dashboard-content clear-fix">		
			<g:layoutBody/>
		</div>
	</section>
	<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
	<r:layoutResources />
</body>
</html>
