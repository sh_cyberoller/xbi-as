<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		
		<title><g:layoutTitle default="XBI"/></title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
				
		<!--CSS first-->
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'flat-ui.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'demo.css')}" type="text/css">
		<!--javascript first-->
		<g:javascript src="jquery-1.8.3.min.js"></g:javascript>
		<g:javascript src="jquery-ui-1.10.3.custom.min.js"></g:javascript>
		<g:javascript src="jquery.ui.touch-punch.min.js"></g:javascript>
		<g:javascript src="bootstrap.min.js"></g:javascript>
		<g:javascript src="bootstrap-select.js"></g:javascript>
		<g:javascript src="bootstrap-switch.js"></g:javascript>
		<g:javascript src="flatui-checkbox.js"></g:javascript>
		<g:javascript src="flatui-radio.js"></g:javascript>
		<g:javascript src="jquery.tagsinput.js"></g:javascript>
		<g:javascript src="jquery.placeholder.js"></g:javascript>

		<g:layoutHead/>
		<g:javascript library="application"/>
		<r:layoutResources />
	</head>
	<body>
		
		<g:layoutBody/>
			

		<g:javascript library="application"/>
		<r:layoutResources />
	</body>
</html>