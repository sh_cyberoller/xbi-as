<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		
		<title><g:layoutTitle default="XBI"/></title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
		
		<!--CSS first-->
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'birt.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'mobile.css')}" type="text/css">
		<r:require module="pure-menus" />
		<!--javascript first-->
		<g:javascript src="jquery-1.8.2.min.js"/>
		
		<r:layoutResources />
		<!--javascript second-->
		<g:javascript src="JQueryBlockUI.js"/>
		<g:javascript src="jquery.blockUI.dialog.js" />
		<g:javascript src="my-numeric.js" />
		<shiro:isLoggedIn>
		<script type="text/javascript">
			//退出确认
			function configLogout(){
				var url = "${createLink(controller: 'auth', action : 'signOut')}";
				$("a[href='"+url+"']").attr("href", "javascript:void(0)").click(function(){
					$.confirm({
						message: "确定要退出吗？",
						okText: "确定",
						cancelText: "取消",
						css: {
							"background-color": "white",
							"color": "red"
						},
						onOk: function () {
							window.location.href = "${createLink(controller: 'auth', action : 'signOut')}"
						},
                        onCancel: function () {
                            $.unblockUI();
                        }
					});
				});
			}

			function resize(){
				////浏览器时下窗口可视区域高度
				var main_height = $(".main").height()
				var min_height = $(window).height().sub($(".footer").height()).sub($("#mainMenu").height())
				$(".main").css("min-height",min_height.toString() + "px");
			}
			
			$(document).ready(function(){
				//加载菜单
				$.ajax({
					url: "${createLink( controller:'nav', action:'menu', params:[menuPath : controllerName])}"
				}).done(function(data) {
					$("#mainMenu").html( data );
					//退出确认
					configLogout();
					//设置主窗口高度
					resize()//
				});
			});
			$(window).resize(function() {
				resize()
			});
		</script>
		</shiro:isLoggedIn>
		<g:layoutHead/>
	</head>
	<body>
		<div class="main">
			<!-- header -->
			<div class="header">
				<div class="blank_line"></div>
				<div class="global_title"><g:message code="global.app.name" default="XBI"/></div>
				<div class="blank_line"></div>
			</div>
			<!-- end header -->
			
			<shiro:isLoggedIn>
			<!-- menu -->
			<div id="mainMenu" class="pure-menu pure-menu-open pure-menu-horizontal">菜单加载中...</div>
			<div class="blank_line"></div>
			<!-- endmenu -->
			</shiro:isLoggedIn>

			<!-- content -->
			<g:layoutBody/>
			<!-- end content -->
		</div>
		<!-- footer -->
		<div class="footer">
			<div class="blank_line"></div>
		</div>

		<g:javascript library="application"/>
		<r:layoutResources />
	</body>
</html>