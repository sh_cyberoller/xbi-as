<nav:set scope="${menuScope}"/>
<g:if test="${menuScope == 'adm'}">
	<g:if test="${menuPath == 'birt'}">
		 <nav:set path="${menuScope}/report"/>
	</g:if>
	<g:elseif test="${menuPath == 'as001'}">
		<nav:set path="${menuScope}/dashboard"/>
	</g:elseif>
	<g:elseif test="${menuPath == 'xfile'}">
		<nav:set path="${menuScope}/xfolder"/>
	</g:elseif>
	<g:else>
		 <nav:set path="${menuScope}/${menuPath}"/>
	</g:else>
</g:if>
<g:if test="${menuScope == 'usr'}">
	<g:if test="${menuPath == 'birt'}">
		 <nav:set path="${menuScope}/open"/>
	</g:if>
	<g:elseif test="${menuPath == 'dashboard'}">
		<nav:set path="${menuScope}/open"/>
	</g:elseif>
	<g:else>
		 <nav:set path="${menuScope}/${menuPath}"/>
	</g:else>
</g:if>

<nav:menu class="" custom="true" scope="${menuScope}">
	<li class="${item.data.icon ? item.data.icon : ''}">
		<p:callTag tag="g:link" attrs="${linkArgs + [class:active ? 'pure-menu-selected' : '']}">
			<nav:title item="${item}"/>
		</p:callTag>
		<g:if test="${item.children}">
			<nav:menu scope="${item.id}" custom="true" class="">
				<li class="${item.data.icon ? 'i_'+item.data.icon : ''}">
					<p:callTag tag="g:link" attrs="${linkArgs}">
						<nav:title item="${item}"/>
					</p:callTag>
				</li>
			</nav:menu>
		</g:if>
	</li>
</nav:menu>