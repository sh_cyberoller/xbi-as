<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		
		<title><g:layoutTitle default="XBI"/></title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<!--CSS first-->
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'flat-ui.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'demo.css')}" type="text/css">
		
		<g:if test="${controllerName != 'birt'}">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'flat-ui-table.css')}" type="text/css">		
		</g:if>
		
		
		<!--javascript first-->
		<g:javascript src="jquery-1.8.3.min.js"></g:javascript>
		<g:javascript src="jquery-ui-1.10.3.custom.min.js"></g:javascript>
		<g:javascript src="jquery.ui.touch-punch.min.js"></g:javascript>
		<g:javascript src="bootstrap.min.js"></g:javascript>
		<g:javascript src="bootstrap-select.js"></g:javascript>
		<g:javascript src="bootstrap-switch.js"></g:javascript>
		<g:javascript src="flatui-checkbox.js"></g:javascript>
		<g:javascript src="flatui-radio.js"></g:javascript>
		<g:javascript src="jquery.tagsinput.js"></g:javascript>
		<g:javascript src="jquery.placeholder.js"></g:javascript>
		<g:javascript src="JQueryBlockUI.js"/>
		<g:javascript src="jquery.blockUI.dialog.js" />
		<g:javascript src="my-numeric.js" />
		<r:layoutResources />
		<!--javascript second-->
		<script type="text/javascript">

			function resize(){
				////浏览器时下窗口可视区域高度
				var height = $(window).height();
				var footer_height = $(".footer").height()
				$(".main").css("min-height", height.sub(footer_height).toString() + "px");
			}
			//退出确认
			function configLogout(){
				var url = "${createLink(controller: 'auth', action : 'signOut')}";
				$("a[href='"+url+"']").attr("href", "javascript:void(0)").click(function(){
					$.confirm({
						message: "确定要退出吗？",
						okText: "确定",
						cancelText: "取消",
						css: {
							"background-color": "white",
							"color": "red"
						},
						onOk: function () {
							window.location.href = "${createLink(controller: 'auth', action : 'signOut')}"
						},
                        onCancel: function () {
                            $.unblockUI();
                        }
					});
				});
			}
			
			$(document).ready(function(){
				configLogout();
				resize();
			});

			$(window).resize(function() {
				resize()
			});

		</script>
		<g:layoutHead/>
	</head>
	<body>
		<div class="main">
		
			<nav class="navbar navbar-default" role="navigation">
			
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
						<span class="sr-only">Toggle navigation</span>
					</button>
					<a class="navbar-brand" href="${createLink(uri:'/')}">XBI</a>
				</div>
	
	
				<div class="collapse navbar-collapse" id="navbar-collapse-01">
					<ul class="nav navbar-nav">
						
						<g:include controller="home" action="homeDashboard" />
						<g:include controller="home" action="menuList" />
						<li>
							<a href="${createLink(controller:'open', action:'index')}">
								<g:message code="nav.usr.open" default="Open"/>
							</a>
						</li>
			
						<li>
							<a href="${createLink(controller:'auth', action:'signOut')}">
								<g:message code="nav.usr.signOut" default="Sign Out"/>
							</a>
						</li>
						
						
					</ul>
					<p class="navbar-text navbar-right"><g:link controller="${controllerName}" action="${actionName}" params="[lang:'en']" id="${params?.id}">English</g:link></p>
					<p class="navbar-text navbar-right"><g:link controller="${controllerName}" action="${actionName}" params="[lang:'zh_CN']" id="${params?.id}">中文</g:link></p>
				</div>
	
			</nav>


			<g:include controller="home" action="usermenu" ></g:include>

			<!-- content -->
			<g:layoutBody/>
			<!-- end content -->
		</div>
		<!-- footer -->
		<div class="footer" id="pageFooter">
			<div class="blank_line"></div>
		</div>

		<g:javascript library="application"/>
		<r:layoutResources />
	</body>
</html>