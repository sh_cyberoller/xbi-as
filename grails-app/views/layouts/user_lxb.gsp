<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		
		<title><g:layoutTitle default="XBI"/></title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<!--CSS first-->
		<link rel="stylesheet" href="${resource(dir: 'css/user_lxb', file: 'bootstrap.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css/user_lxb', file: 'font-awesome.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css/user_lxb', file: 'left-sidebar.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css/user_lxb', file: 'navbar.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css/user_lxb', file: 'todc-bootstrap.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css/user_lxb', file: 'layout.css')}" type="text/css">		
		
		<g:if test="${controllerName != 'birt'}">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'flat-ui-table.css')}" type="text/css">		
		</g:if>
		
		<!--javascript first-->
		<script type="text/javascript" src="${resource(dir: 'js/user_lxb', file: 'jquery-1.11.0.js')}" ></script>
		<script type="text/javascript" src="${resource(dir: 'js/user_lxb', file: 'function.js')}" ></script>
		<script type="text/javascript" src="${resource(dir: 'js/user_lxb', file: 'bootstrap.js')}" ></script>
		<script type="text/javascript" src="${resource(dir: 'js/user_lxb', file: 'bootbox.js')}" ></script>

		<g:javascript src="jquery-ui-1.10.3.custom.min.js"></g:javascript>
		<g:javascript src="JQueryBlockUI.js"/>
		<g:javascript src="jquery.blockUI.dialog.js" />
		<g:javascript src="my-numeric.js" />
		<r:layoutResources />
		<!--javascript second-->
		<script type="text/javascript" src="${resource(dir: 'js', file: 'globalize.min.js')}" ></script>
		<script type="text/javascript" src="${resource(dir: 'js', file: 'dx.chartjs.js')}" ></script>
		<script type="text/javascript" src="${resource(dir: 'js', file: 'jquery-ui-numeric-min.js')}" ></script>

		<script type="text/javascript">

			function resize(){
				////浏览器时下窗口可视区域高度
				var height = $(window).height();
				var footer_height = $(".footer_con").height()
				$(".main-content").css("min-height", height.sub(footer_height).toString() + "px");
			}
			//退出确认
			function configLogout(){
				var url = "${createLink(controller: 'auth', action : 'signOut')}";
				$("a[href='"+url+"']").attr("href", "javascript:void(0)").click(function(){
					$.confirm({
						message: "确定要退出吗？",
						okText: "确定",
						cancelText: "取消",
						css: {
							"background-color": "white",
							"color": "red"
						},
						onOk: function () {
							window.location.href = "${createLink(controller: 'auth', action : 'signOut')}"
						},
                        onCancel: function () {
                            $.unblockUI();
                        }
					});
				});
			}

			function loadDashboard(dashboard_id){
				var url = "${createLink(controller:'open', action:'loadDashboard')}";
				$.ajax({
					url: [url, '/', dashboard_id].join("")
				  , dataType: "html"
				}).done( function(html){
					$("#layoutBody").html(html);
				}).fail(function(jqXHR, textStatus, errorThrown){
					debugger;
					$("#layoutBody").html("错误");
				});
			}
			
			$(document).ready(function(){
				configLogout();
				resize();
			});

			$(window).resize(function() {
				resize();
			});

		</script>
		<g:layoutHead/>
	</head>
	<body class="navbar-fixed">
		<div class="navbar navbar-default navbar-fixed-top" id="navbar">
			<div class="navbar-container" id="navbar-container">
				<div class="navbar-header pull-left">
					<a href="javascript:void(0);" class="navbar-brand">
						<small style="color: white">
							XBI-五项管理demo&nbsp;<span class="badge">Beta</span>
						</small>
					</a>
				</div>
				<div class="navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						<li class="light-blue">
							<a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle">
								<img class="nav-user-photo" src="${resource(dir: 'images/user_lxb', file: 'user_logo.jpg')}" alt="User's Photo" />
								<span class="user-info">
									<small>你好,</small>
									<shiro:principal/>
								</span>
								<i class="fa fa-caret-down"></i>
							</a>
							<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<g:link controller="${controllerName}" action="${actionName}" params="[lang:'en']" id="${params?.id}"><i class="fa fa-cog"></i>English</g:link>
								</li>
								<li>
									<g:link controller="${controllerName}" action="${actionName}" params="[lang:'zh_CN']" id="${params?.id}"><i class="fa fa-user"></i>中文</g:link>
								</li>
								<li class="divider"></li>
								<li>
									<a href="${createLink(controller:'auth', action:'signOut')}">
										<i class="fa fa-sign-out"></i>
										<g:message code="nav.usr.signOut" default="Sign Out"/>
									</a>
								</li>
							</ul>
						</li>
					</ul> 
				</div> 
			</div> 
		</div>
		
		<div class="main-container" id="main-container">
			<div class="main-container-inner">
		
				<div class="sidebar sidebar-fixed" id="sidebar">

					<ul class="nav nav-list">
						<li>
							<a data-has-menu="true" href="${createLink(uri:'/')}" id="menu-shouye-school" class="dropdown-toggle">
								<i class="fa fa-home"></i>
								<span class="menu-text-in">首页</span>
							</a>
						</li>
						
						<g:include controller="home" action="rootFolder" />

					</ul>
					<!-- /.nav-list -->

					<div class="sidebar-collapse" id="sidebar-collapse">
						<i class="fa fa-angle-double-left" data-icon1="fa-angle-double-left" data-icon2="fa-angle-double-right"></i>
					</div>

				</div>
		
				<div class="main-content" id="layoutBody">
					<!-- content -->
					<g:layoutBody/>
					<!-- end content -->
				</div>

				<div>
					<div class="row col-xs-12">
						<div class="footer_con">
							Copyright@ 2013 上海扬邑企业管理咨询有限公司     沪ICP备10221484号-3
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- footer -->
		<div class="footer" id="pageFooter">
			<div class="blank_line"></div>
		</div>

		<g:javascript library="application"/>
		<r:layoutResources />
	</body>
</html>