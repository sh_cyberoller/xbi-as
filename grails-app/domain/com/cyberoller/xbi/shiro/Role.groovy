package com.cyberoller.xbi.shiro

class Role {
	//角色名称
    String name
	//别名
    String alias
	//描述
    String description
	//创建日期
	Date dateCreated
	//修改日期
	Date lastUpdated

    static hasMany = [ users: User, permissions: String ]
    static belongsTo = User

    static constraints = {
        name(nullable: false, blank: false, unique: true, maxSize : 50)
        alias(nullable:false, blank:false, unique : true, maxSize : 3)
        description(nullable:true, blank:true, maxSize : 200)
    }

	def beforeValidate(){
    	name = name?.trim()?.toUpperCase()
		alias = alias?.trim()?.toUpperCase()
    	description = description?.trim()
    }

	String toString(){
    	return "${alias}-${name}"
    }

	static mapping = {
    	table "shiro_role"
    }
}
