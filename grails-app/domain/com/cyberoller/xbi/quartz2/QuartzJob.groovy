package com.cyberoller.xbi.quartz2

class QuartzJob {

	String name
	Boolean isActive

    static constraints = {
    	name (blank:false, nullable:false, unique:true, maxSize: 50)
    	isActive(nullable:false)
    }
    
    def beforeValidate(){
    	//如果启用为NULL，则更新为FALSE
    	if(this.isActive == null){
    		this.isActive = false
    	}
    }

    String toString(){
    	return "${username}"
    }
    
    static mapping = {
    	table "quartz2_jobs"
    }
}
