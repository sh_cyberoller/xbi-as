package com.cyberoller.xbi

import com.cyberoller.xbi.shiro.User

/*文件夹*/
class Xfolder {

	static hasMany = [xfiles : Xfile]

	//父文件夹
	Xfolder xfolder
	//序号
	String serialNumber
	//名称
	String name
	//图标
	String icon
	//备注
	String description
	//所有人
	User owner
	//创建人
	User createdBy
	//修改人
	User lastModifiedBy
	//创建日期
	Date dateCreated
	//修改日期
	Date lastUpdated

    static constraints = {
		xfolder(nullable:true)
		serialNumber(nullable:false, blank:false, maxSize:4, unique:'xfolder')
		name(nullable:false, blank:false, maxSize : 50)
		icon(nullable:false, blank:false, maxSize : 100)
		description(nullable:true, blank:true, maxSize : 200)
		owner(nullable:false)
		createdBy(nullable:false)
		lastModifiedBy(nullable:false)
    }

    String toString(){
    	return "${name}"
    }
    
    static mapping = {
        table "xbi_folder"
    }
}