package com.cyberoller.xbi

import com.cyberoller.xbi.shiro.User
import com.cyberoller.xbi.birt.Report
import com.cyberoller.xbi.chartjs.Dashboard

/*文件*/
class Xfile {

	static belongsTo = [xfolder : Xfolder]

	//文件夹
	Xfolder xfolder
	//序号
	String serialNumber
	//类型
	String xtype
	//报表
	Report report
	//仪表盘
	Dashboard dashboard
	//所有人
	User owner
	//创建人
	User createdBy
	//修改人
	User lastModifiedBy
	//创建日期
	Date dateCreated
	//修改日期
	Date lastUpdated

    static constraints = {
		xfolder(nullable:true)
		serialNumber(nullable:false, unique : 'xfolder', maxSize : 4)
		xtype(nullable:false, blank:false, maxSize : 30, inList: ["Report", "Dashboard"])
		report(nullable:true)
		dashboard(nullable:true)
		owner(nullable:false)
		createdBy(nullable:false)
		lastModifiedBy(nullable:false)
    }

    String toString(){
    	return "${serialNumber}"
    }
    
    static mapping = {
        table "xbi_file"
    }
}
