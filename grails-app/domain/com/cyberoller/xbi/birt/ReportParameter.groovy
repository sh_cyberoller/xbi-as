package com.cyberoller.xbi.birt

class ReportParameter {

	Report report
    String name
    String datatype
    String prompttext
    Boolean allowblank
    Boolean hidden
    
	Date dateCreated
	Date lastUpdated

    static belongsTo = [report: Report]

    static constraints = {
        report(nullable:false)
        name(nullable:false, blank:false, unique : 'report')
        datatype(nullable:false, maxSize:50)
        prompttext(nullable:true, maxSize: 200)
        allowblank(nullable:true)
        hidden(nullable:true)
    }
    
    static mapping = {
        table "birt_report_parameter"
    }
}