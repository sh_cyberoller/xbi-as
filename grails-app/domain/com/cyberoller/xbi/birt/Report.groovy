package com.cyberoller.xbi.birt

import com.cyberoller.xbi.shiro.User

class Report {
	
	//报表参数
	static hasMany = [ reportParameters : ReportParameter ]

	//名称
    String name
	//IDE
    String author
	//标题
    String title
	//创建人
    String ide
	//描述
    String description
    String comment
	//
    String units
	//
    String helpGuide
	//文件名称
    String filename
	//文件位置
    String fullfile

	//所有人
	User owner
	//创建人
	User createdBy
	//修改人
	User lastModifiedBy

	//创建日期
	Date dateCreated
	//修改日期
	Date lastUpdated
    
    static constraints = {
		name(nullable:false, blank:false, unique : true, maxSize: 200)
		author(nullable:true, blank:true, maxSize : 200)
		title(nullable:true, blank:true, maxSize : 200)
		ide(nullable:true, blank:true, maxSize : 200)
		description(nullable:true, blank:true, maxSize : 200)
		comment(nullable:true, blank:true, maxSize : 200)
		units(nullable:true, blank:true, maxSize : 200)
		helpGuide(nullable:true, blank:true, maxSize : 200)
		filename(nullable:true, blank:true, maxSize : 200)
		fullfile(nullable:true, blank:true, maxSize : 200)

		owner(nullable:false)
		createdBy(nullable:false)
		lastModifiedBy(nullable:false)
    }
    
    String toString(){
    	return "${name}"
    }
    
    static mapping = {
        table "birt_report"
    }
}