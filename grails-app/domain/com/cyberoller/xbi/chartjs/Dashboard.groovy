package com.cyberoller.xbi.chartjs

import com.cyberoller.xbi.shiro.User

class Dashboard {
	
	//名称
	String name
	//作者
	String author
	//标题
	String title
	//数据源
	DataSource datasource
	//描述
	String description
	//放在菜单上
	Boolean atMenu
	//菜单顺序
	String menuOrder
	//放在主页
	Boolean homePage
	//启用
	Boolean isActive
	//所有人
	User owner
	//创建人
	User createdBy
	//修改人
	User lastModifiedBy
	//创建日期
	Date dateCreated
	//修改日期
	Date lastUpdated

    static constraints = {
		name(nullable:false, blank:false, unique : true, maxSize: 20)
		author(nullable:true, blank:true, maxSize : 20)
		title(nullable:true, blank:true, maxSize : 200)
		datasource(nullable:false)
		description(nullable:true, blank:true, maxSize : 200)
		atMenu(nullable:false)
		menuOrder(nullalbe:true, maxSize : 2)
		homePage(nullable:false)
		isActive(nullable:false)
    	owner(nullable:false)
    	createdBy(nullable:false)
    	lastModifiedBy(nullable:false)
    }
    
	def beforeValidate(){
    	//名称换成小写
    	name = name?.trim()?.toLowerCase()
    	//如果启用为NULL，则更新为FALSE
    	if(this.isActive == null){
    		this.isActive = false
    	}
		if(this.homePage == null){
    		this.homePage = false
    	}
		if(this.atMenu == null){
    		this.atMenu = false
    	}
    }


    String toString(){
    	return "${name}"
    }
    
    static mapping = {
        table "chartjs_dashboard"
    }
}