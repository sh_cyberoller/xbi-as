package com.cyberoller.xbi.chartjs

import com.cyberoller.xbi.shiro.User

//数据库连接
class DataSource {

	//名称
	String name
	//地址
	String url
	//用户名
	String username
	//密码
	String password
	//驱动
	String driver
	//描述
	String description
	//所有人
	User owner
	//创建人
	User createdBy
	//修改人
	User lastModifiedBy
	//创建日期
	Date dateCreated
	//修改日期
	Date lastUpdated


    static constraints = {
    	name(nullable:false, blank:false, maxSize : 20)
    	url(nullable:false, blank:false, unique:true, maxSize : 200)
    	username(nullable:false, blank:false, maxSize : 20)
    	password(nullable:false, blank:false, maxSize : 20)
    	driver(nullable:false, blank:false, maxSize : 100)
    	description(nullable:true, blank:true, maxSize : 200)
    	owner(nullable:false)
    	createdBy(nullable:false)
    	lastModifiedBy(nullable:false)
    }
    
	def beforeValidate(){
    	//名称换成小写
    	name = name?.trim()?.toLowerCase()
    	password = password?:""
    }

    
    String toString(){
    	return "${name}"
    }
    
    static mapping = {
        table "chartjs_datasource"
    }

}
