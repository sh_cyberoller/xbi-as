package com.cyberoller.xbi.chartjs

import org.springframework.transaction.annotation.Transactional

import org.hibernate.criterion.CriteriaSpecification

import org.apache.shiro.SecurityUtils
import com.cyberoller.xbi.shiro.User

import groovy.sql.Sql
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource

import java.sql.ResultSet
import java.sql.ResultSetMetaData

import groovy.json.JsonBuilder;

class DashboardService {

	static transactional = false

	def birtReportService

	/*获取当前用户ID*/
	@Transactional(readOnly = true)
    def currentUserId() {
		def currentUserId
		try{
			def userInstance = User.withCriteria(uniqueResult:true){
				eq("username", SecurityUtils.getSubject().getPrincipal())
			}
			if(!userInstance){
				throw new RuntimeException("获取当前用户失败")
			}
			currentUserId = userInstance.id
		}catch(e){
			throw new RuntimeException("获取当前用户ID失败错误：${e.getMessage()}")
		}
		return currentUserId
    }

	/*切换是否启用*/
	def toggleActive(Long id, Long version){
	    try{
			def dashboardInstance = Dashboard.withCriteria(uniqueResult:true){
				eq("id", id)
			}
			if(!dashboardInstance){
				throw new RuntimeException("获取仪表盘${id}失败")
			}
			
			if(dashboardInstance.version > version){
				throw new RuntimeException("仪表盘${id}已经被更新，请刷新页面")
			}

			dashboardInstance.isActive = dashboardInstance.isActive?false:true

			dashboardInstance.save(flush:true);
		}catch(e){
			throw new RuntimeException("获取当前用户ID失败错误：${e.getMessage()}")
		}
	}

	/*获取数据*/
    def data(Long id, String sql){
    	log.debug "**********取數據入口"
		def data = ""
		def sqlInstance
		try{
			log.debug "**********獲取儀表盤名稱和數據源"
			def dashboardInstance = Dashboard.withCriteria(uniqueResult:true){
				createAlias("datasource", "datasource")
				projections{
					property("name", "name")
					property("datasource.id", "datasourceId")
				}
				join("datasource")
				eq("id", id)

				resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
			}
			if(!dashboardInstance){
				throw new RuntimeException("仪表盘${id}不存在")
			}
			log.debug "**********獲取數據源"
			def dataSourceInstance = DataSource.withCriteria(uniqueResult:true){
				projections{
					property("url", "url")
					property("username", "user")
					property("password", "password")
				}
				eq("id", dashboardInstance.datasourceId)

				resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
			}
			if(!dataSourceInstance){
				throw new RuntimeException("获取数据源:${id}失败")
			}
			log.debug "**********連接數據源"
			//http://www.chemaxon.com/instantjchem/ijc_latest/docs/developer/scriptlets/connect_to_db.html
			MysqlDataSource ds = new MysqlDataSource()
			ds.user = dataSourceInstance.user
			ds.password = dataSourceInstance.password
			ds.url = dataSourceInstance.url
			sqlInstance = new Sql(ds)
			log.debug "**********已連接，開始執行語句 : call ${sql}"
			//rs = sqlInstance.executeQuery("""call ${dashboardInstance.name}()""")eachRow
			def list = []
			String sqlString = "call ${sql}"
			sqlInstance.query (sqlString) {ResultSet rs ->
				while (rs.next()){
					ResultSetMetaData metaData = rs.getMetaData();
					def map = [:]
					for (int i = 1; i <= metaData.getColumnCount(); i++) {
						String columnName = metaData.getColumnLabel(i)
						log.debug "**********字段名：${columnName}"
						String columnType = metaData.getColumnTypeName(i)
						log.debug "**********字段數據類型：${columnType}"
						//http://docs.oracle.com/javase/1.5.0/docs/api/java/sql/Types.html
						switch(columnType){
							case "BIGINT" :
							case "INTEGER":
								map.put(columnName,rs.getLong(columnName))
								break
							case "DOUBLE" :
							case "FLOAT" :
							case "REAL" :
								map.put(columnName,rs.getDouble(columnName))
								break
							case "DECIMAL":
								map.put(columnName,rs.getBigDecimal(columnName))
								break
							case "CHAR":
								map.put(columnName,rs.getString(columnName))
								break
							case "DATE":
								map.put(columnName,rs.getDate(columnName))
								break
							default:
								map.put(columnName,rs.getString(columnName))
								break
						}
					}
					list.add(map)
				}
			}
			log.debug "**********轉成JSON字符串"
			def builder = new JsonBuilder(list)
			data = builder.toString()
			log.debug "**********結果：${data}"
		}catch(e){
			throw new RuntimeException("获取数据失败：${e.getMessage()}")
		}finally{
			if(sqlInstance != null){
				sqlInstance.close()
			}
		}
		return data;
    }

	/*获取模板*/
    def template(Long id, String dashboardHome){
		def content = ""
		try{
			def encoding = "UTF-8";//定义编码格式
			def dashboardInstance = Dashboard.withCriteria(uniqueResult:true){
				createAlias("datasource", "datasource")
				projections{
					property("name", "name")
				}
				join("datasource")
				eq("id", id)

				resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
			}

			if(!dashboardInstance){
				throw new RuntimeException("仪表盘${id}不存在")
			}

			def file_name = "${dashboardHome}/${dashboardInstance.name}.html"
			File file = new File(file_name)

			if(!file.isFile() || !file.exists()) {//判断文件是否存在
				throw new RuntimeException("模板${file_name}不存在")
			}

			InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(file));
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

			StringBuilder sbData = new StringBuilder();
			String data = "";
			while((data = bufferedReader.readLine()) != null) {
				sbData.append(data);
			}
			bufferedReader.close();
			inputStreamReader.close();
			content = sbData.toString();
		}catch(e){
			throw new RuntimeException("获取模板失败：${e.getMessage()}")
		}
		return content
    }
}