package com.cyberoller.xbi.chartjs

import org.springframework.transaction.annotation.Transactional

import org.hibernate.criterion.CriteriaSpecification

import org.apache.shiro.SecurityUtils
import com.cyberoller.xbi.shiro.User

import groovy.sql.Sql

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource

class DataSourceService {

	static transactional = false

	@Transactional(readOnly = true)
    def currentUserId() {
		def currentUserId
		try{
			def userInstance = User.withCriteria(uniqueResult:true){
				eq("username", SecurityUtils.getSubject().getPrincipal())
			}
			if(!userInstance){
				throw new RuntimeException("获取当前用户失败")
			}
			currentUserId = userInstance.id
		}catch(e){
			throw new RuntimeException("获取当前用户ID失败错误：${e.getMessage()}")
		}
		return currentUserId
    }

	/*测试数据库连接*/
	@Transactional(readOnly = true)
	def testConnection (Long id){
		//http://groovy.codehaus.org/api/groovy/sql/Sql.html
		def sqlInstance
		try{
			def dataSourceInstance = DataSource.withCriteria(uniqueResult:true){
				projections{
					property("url", "url")
					property("username", "user")
					property("password", "password")
					property("driver", "driver")
				}
				eq("id", id)

				resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
			}

			if(!dataSourceInstance){
				throw new RuntimeException("获取dataSource:${id}失败")
			}

			//http://www.chemaxon.com/instantjchem/ijc_latest/docs/developer/scriptlets/connect_to_db.html
			MysqlDataSource ds = new MysqlDataSource()
			ds.user = dataSourceInstance.user
			ds.password = dataSourceInstance.password
			ds.url = dataSourceInstance.url

			sqlInstance = new Sql(ds)
			sqlInstance.getConnection()
		}catch(e){
			throw new RuntimeException(e.getMessage())
		}finally{
			if(sqlInstance != null){
				sqlInstance.close()
			}
		}
	}
}
