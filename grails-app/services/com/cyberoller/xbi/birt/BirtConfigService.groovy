package com.cyberoller.xbi.birt

import org.springframework.transaction.annotation.Transactional

import com.cyberoller.xbi.shiro.User

import org.apache.shiro.SecurityUtils

class BirtConfigService {

    static transactional = true

    @Transactional(readOnly = true)
    def currentUserId() {
		def userId
		try{
			userId = User.withCriteria(uniqueResult:true){
				projections{
					property("id")
				}
				ilike("username", SecurityUtils.getSubject().getPrincipal())
			}
		}catch(e){
			throw new RuntimeException("获取用户ID失败：${e.getMessage()}")
		}
		return userId
    }
}