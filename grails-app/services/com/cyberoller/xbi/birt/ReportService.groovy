package com.cyberoller.xbi.birt

import org.springframework.transaction.annotation.Transactional

import org.apache.shiro.SecurityUtils
import com.cyberoller.xbi.shiro.User

class ReportService {

    static transactional = false
    /**
     *因为有多个角色都要使用该对象，不同的角色分配不同的页面
     **/
	@Transactional(readOnly = true)
	def layout(){
		def layout
		try{
			def userInstance = User.findByUsername(SecurityUtils.getSubject().getPrincipal())
			layout = userInstance.roleAlias.toLowerCase()
		}catch(e){
			throw new RuntimeException( "获取页面布局错误：${e.getMessage()}" )
		}
		return layout
	}

	/*获取当前用户ID*/
	@Transactional(readOnly = true)
    def currentUserId() {
		def currentUserId
		try{
			def userInstance = User.withCriteria(uniqueResult:true){
				eq("username", SecurityUtils.getSubject().getPrincipal())
			}
			if(!userInstance){
				throw new RuntimeException("获取当前用户失败")
			}
			currentUserId = userInstance.id
		}catch(e){
			throw new RuntimeException("获取当前用户ID失败错误：${e.getMessage()}")
		}
		return currentUserId
    }

	@Transactional(readOnly = true)
    def list(Object params) {
        def reportInstanceList = Report.withCriteria{
        	//最大返回行数
			if(params?.max) maxResults(params.int("max"))
			//偏移量
			if(params?.offset) firstResult(params.int("offset"))
			//排序字段
			if(params?.sort && params?.order) order(params?.sort, params?.order)
		}
        return reportInstanceList
    }
    
    @Transactional(readOnly = true)
    def count() {
        def reportInstanceCount = Report.withCriteria{
			projections{
				rowCount()
			}
		}
        return (reportInstanceCount == null)? 0: reportInstanceCount[0]
    }
    
    @Transactional(readOnly = true)
    def show(id) {
        def reportInstance = Report.withCriteria(uniqueResult:true){
            eq("id", id.toLong())
        }
        return reportInstance
    }
}