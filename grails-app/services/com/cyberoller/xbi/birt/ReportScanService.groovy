package com.cyberoller.xbi.birt

import org.codehaus.groovy.grails.web.metaclass.BindDynamicMethod
import com.cyberoller.xbi.shiro.User

class ReportScanService {

    static transactional = false

    def birtReportService
    
	//扫描报表目录，增加或者删除报表
    def scan(Long userId, String filename) {
		log.info("userId=${userId}")

		def userInstance = User.withCriteria(uniqueResult:true){
			eq("id", userId)
		}

        def reports = birtReportService.listReports()
        def reportMap = [:];
        reports.each{
			log.debug "filename=${filename}"
			log.debug "it.file=${it.file}"
			if(filename == it.file){
				reportMap.put(it.name, it.name)
				Report.withTransaction{ status ->
					try{
						def reportInstance = Report.findByName(it.name)
						if(!reportInstance){
							reportInstance = new Report()
						}
						reportInstance.author = it.author?:""
						reportInstance.title = it.title?:""
						reportInstance.ide = it.createdBy?:""
						reportInstance.description = it.description?:""
						reportInstance.comment=it.comment?:""
						reportInstance.units = it.units?:""
						reportInstance.name = it.name?:""
						reportInstance.filename = it.file?:""
						reportInstance.fullfile = it.fullfile?:""
						reportInstance.helpGuide = it.helpGuide?:""

						/*
						BindDynamicMethod mybind = new BindDynamicMethod()
						def mymap = ["lastModifiedBy.id" : userId, "createdBy.id" : userId, "owner.id" : userId]
						def myargs =  [reportInstance, mymap]
						mybind.invoke(reportInstance, 'bind', (Object[]) myargs)
						*/
						reportInstance.lastModifiedBy = userInstance
						reportInstance.createdBy = userInstance
						reportInstance.owner = userInstance
						log.info("reportInstance.lastModifiedBy.id=${reportInstance.lastModifiedBy.id}")
						log.info("reportInstance.createdBy.id=${reportInstance.createdBy.id}")
						log.info("reportInstance.owner.id=${reportInstance.owner.id}")

						reportInstance.save(flush:true)
						def params = birtReportService.getReportParams(reportInstance.name)

						def dbParams = ReportParameter.findAllByReport(reportInstance)

						//在数据库里找到已经被删除的参数，并删除它
						dbParams.each{ paramInstance ->
							def findParam = false
							params?.each {
								if(paramInstance.name == it.name){
									findParam = true
									return true
								}else{
									return false
								}
							}
							if(findParam == false){
								paramInstance.delete(flush:true)
							}
						}
						//将报表参数插入到数据库中
						params.each {
							def paramInstance = ReportParameter.findByReportAndName(reportInstance, it.name )
							if(!paramInstance){
								paramInstance = new ReportParameter()
								paramInstance.report = reportInstance
								paramInstance.name= it.name?:""
							}
							paramInstance.datatype = it.type?:"";
							paramInstance.prompttext = it.promptText?:it.name;
							paramInstance.allowblank = it.allowBlank?:false;
							paramInstance.hidden = it.hidden?:false;
							paramInstance.save(flush:true)
						}
					}catch(e){
						status.setRollbackOnly()
						log.error("扫描报表目录失败： ${e}")
					}
				}
			}
        }
        //清理已经被删除报表的收藏和历史
        Report.list().each{reportInstance ->
        	if(!reportMap.containsKey(reportInstance.name)){
				Report.withTransaction{ status ->
					try{
						ReportFolderItem.findAllByReport(reportInstance).each{ folderItem ->
							ReportRecentItem.findAllByReportFolderItem(folderItem).each{ recentItem ->
								recentItem.delete(flush:true)
							}
							ReportFavoriteItem.findAllByReportFolderItem(folderItem).each{ favoriteItem ->
								favoriteItem.delete(flush:true)
							}
							folderItem.delete(flush:true)
						}
						reportInstance.delete(flush:true)
					}catch(e){
						status.setRollbackOnly()
						log.error("删除报表失败, ${e}")
					}
				}
        	}
        }
        

    }
    
    def listReportForParameter(parameterName){
        return Report.createCriteria().list{
            reportParameters {
                like('name', parameterName)
            }
        }
    }
}