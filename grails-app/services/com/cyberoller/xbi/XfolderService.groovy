package com.cyberoller.xbi

import org.springframework.transaction.annotation.Transactional

import org.hibernate.criterion.CriteriaSpecification

import org.apache.shiro.SecurityUtils
import com.cyberoller.xbi.shiro.User

class XfolderService {

	static transactional = false

	@Transactional(readOnly = true)
    def currentUserId() {
		def currentUserId
		try{
			def userInstance = User.withCriteria(uniqueResult:true){
				eq("username", SecurityUtils.getSubject().getPrincipal())
			}
			if(!userInstance){
				throw new RuntimeException("获取当前用户失败")
			}
			currentUserId = userInstance.id
		}catch(e){
			throw new RuntimeException("获取文件夹列表错误：${e.getMessage()}")
		}
		return currentUserId
    }

	@Transactional(readOnly = true)
    def list(Object params) {
		def XfolderInstanceList
		try{
			XfolderInstanceList = Xfolder.withCriteria{
				//最大行数
				if(params?.max) maxResults(params.int('max'))
				//偏移量
				if(params?.offset) firstResult(params.int('offset'))
				//排序
				if(params?.sort && params?.order){
					 order(params?.sort, params?.order)
				}
			}
		}catch(e){
			throw new RuntimeException("获取文件夹列表错误：${e.getMessage()}")
		}
		return XfolderInstanceList
    }
    
    //计算数量
    @Transactional(readOnly = true)
    def count(Object params) {
		def XfolderInstanceCount = 0
		try{
			XfolderInstanceCount = Xfolder.withCriteria(uniqueResult:true){		
				projections{
					count("id")
				}
			}
		}catch(e){
			throw new RuntimeException("获取文件夹数量错误：${e.getMessage()}")
		}
		return XfolderInstanceCount
    }

	//计算路径
	@Transactional(readOnly = true)
	def fullpath (Long folderId){
	    def fullpath = ""
		try{
			//查寻当前文件夹
			def xfolderInstance = Xfolder.withCriteria(uniqueResult:true){
				projections{
					property("id", "id")
					property("name", "name")
				}

				eq("id", folderId)

				resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
			}
			if(!xfolderInstance){
				throw new RuntimeException("未找到文件夹:${folderId}")
			}
			fullpath = "${xfolderInstance.name}"
			//父文件夹
			def parentId = folderId
			while(true){
				def parentInstance = folderParent(parentId)
				
				if(parentInstance){
					parentId = parentInstance.id
					fullpath = "${parentInstance.name}/${fullpath}"
				}else{
					fullpath = "/${fullpath}"
					break
				}
			}
		}catch(e){
			throw new RuntimeException("获取文件夹路径错误：${e.getMessage()}")
		}
		return fullpath
	}

	@Transactional(readOnly = true)
	def folderParent (Long folderId){
	    def parentInstance = ""
		try{
			parentInstance = Xfolder.withCriteria(uniqueResult:true){

				createAlias("xfolder", "parent")

				projections{
					property("parent.id", "id")
					property("parent.name", "name")
				}

				join("xfolder")

				eq("id", folderId)

				resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
			}
		}catch(e){
			throw new RuntimeException("获取父文件夹错误：${e.getMessage()}")
		}
		return parentInstance
	}
}
