package com.cyberoller.xbi

import org.hibernate.criterion.CriteriaSpecification

import org.springframework.transaction.annotation.Transactional
import org.apache.shiro.SecurityUtils

import com.cyberoller.xbi.shiro.User

import com.cyberoller.xbi.chartjs.Dashboard;

class HomeService {

	static transactional = false

	/**
     *获取角色简称
     **/
	@Transactional(readOnly = true)
	def roleAlias(){
		log.debug "獲取用戶角色別名"
		def alias
		try{
			def username = SecurityUtils.getSubject().getPrincipal()
			def userInstance = User.findByUsername(username)
			alias = userInstance.roleAlias.toLowerCase()
		}catch(e){
			throw new RuntimeException("获取用户角色错误：${e.getMessage()}")
		}
		log.debug "**********別名:${alias}"
		return alias
	}
    
    /*獲取主頁儀表盤*/
    @Transactional(readOnly = true)
    def homeDashboard(){
		log.debug "****************************获取主页仪表盘"
    	def dashboardInstance;
		try{
			dashboardInstance = Dashboard.withCriteria(uniqueResult:true){
						
				projections{
					property("id", "id")
					property("title", "title")
				}
				
				eq("atMenu", true)
				eq("homePage", true);

				order("menuOrder")
				
				maxResults(1)

				resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
			}
			log.debug "**********dashboardInstance=${dashboardInstance}"
		}catch(e){
			throw new RuntimeException("获取主页仪表盘错误：${e.getMessage()}")
		}
		return dashboardInstance
    
    }

    
    /*獲取主頁儀表盤*/
    @Transactional(readOnly = true)
    def menuList(){
		log.debug "****************************获取菜单列表，不包括主页"
    	def dashboardInstanceList;
		try{
			dashboardInstanceList = Dashboard.withCriteria(){
						
				projections{
					property("id", "id")
					property("title", "title")
				}
				
				eq("atMenu", true)
				eq("homePage", false);

				order("menuOrder")

				resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
			}
			log.debug "**********dashboardInstanceList=${dashboardInstanceList}"
		}catch(e){
			throw new RuntimeException("获取菜单列表错误：${e.getMessage()}")
		}
		return dashboardInstanceList
    
    }
    /******************************左侧菜单树**********************************************/
	/*获取根目录*/
	@Transactional(readOnly = true)
    def root() {
    	def rootInstance;
		try{
			rootInstance = Xfolder.withCriteria(uniqueResult:true){
				projections{
					property("id", "id")
					property("name", "name")
					property("description", "description")
				}	
				isNull("xfolder.id")
				order("serialNumber")
				maxResults(1)
				resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
			}
		}catch(e){
			throw new RuntimeException("获取根文件夹错误：${e.getMessage()}")
		}
		return rootInstance
    }

	/*获取目录的子目录*/
	@Transactional(readOnly = true)
    def folders(Long folderId) {
		log.debug "****************************folder's id is ${folderId}"
    	def list;
		try{
			list = Xfolder.withCriteria(){
			
				createAlias("xfolder", "parent", CriteriaSpecification.LEFT_JOIN)
			
				projections{
					property("id", "id")
					property("name", "name")
					property("parent.id", "p_id")
					property("icon", "icon")
				}
				
				join("xfolder")
				eq("parent.id", folderId)

				resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
			}
		}catch(e){
			throw new RuntimeException("获取文件夹${folderId}列表错误：${e.getMessage()}")
		}
		return list
    }

	/*获取目录文件*/
    @Transactional(readOnly = true)
	def files(Long folderId){

    	def list;
		try{
			list = Xfile.withCriteria(){
			
				createAlias("xfolder", "folder")
				createAlias("report", "report", CriteriaSpecification.LEFT_JOIN)
				createAlias("dashboard", "dashboard", CriteriaSpecification.LEFT_JOIN)
			
				projections{
					property("id", "id")
					property("folder.name", "folder")
					property("serialNumber", "serialNumber")
					property("xtype", "xtype")
					property("report.name", "report")
					property("report.title", "reportTitle")
					property("report.description", "reportDescription")
					property("dashboard.id", "dashboard")
					property("dashboard.name", "dashboardName")
					property("dashboard.title", "dashboardTitle")
					property("dashboard.description", "dashboardDescription")
				}

				join("xfolder")
				join("report")
				join("dashboard")
				
				or{
					isNotNull("report")
					isNotNull("dashboard")
				}
				
				eq("xfolder.id", folderId)

				order("folder.serialNumber")
				order("serialNumber")

				resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
			}
		}catch(e){
			throw new RuntimeException("获取文件错误：${e.getMessage()}")
		}
		return list
    }    
    
    
}
