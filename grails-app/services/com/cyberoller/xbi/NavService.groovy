package com.cyberoller.xbi

import org.hibernate.criterion.CriteriaSpecification

import org.springframework.transaction.annotation.Transactional
import org.apache.shiro.SecurityUtils

import com.cyberoller.xbi.shiro.User

class NavService {

	static transactional = false

	/**
     *获取角色简称
     **/
	@Transactional(readOnly = true)
	def roleAlias(){
		def alias
		try{
			def username = SecurityUtils.getSubject().getPrincipal()
			def userInstance = User.findByUsername(username)
			alias = userInstance.roleAlias.toLowerCase()
		}catch(e){
			throw new RuntimeException("获取用户角色错误：${e.getMessage()}")
		}
		return alias
	}
}
