import org.apache.shiro.SecurityUtils

def loggedIn = { ->
    SecurityUtils.getSubject().getPrincipal() instanceof String
}

navigation = {
    //管理员
    adm {
        //主页
        home(controller:'home', action:'index', visible: loggedIn, order : 1000)
		//角色
		role(controller:'role', action:'index', visible: loggedIn, order : 1100)
		//用户
		user(controller:'user', action:'index', visible: loggedIn, order : 1200)
		//文件
		xfolder(controller:'xfolder', action:'index', visible: loggedIn, order : 2100)
		//报表
		report(controller:'report', action:'index', visible: loggedIn, order : 2200)
		//数据源
		dataSource(controller:'dataSource', action:'index', visible: loggedIn, order : 2300)
		//仪表盘
		dashboard(controller:'dashboard', action:'index', visible: loggedIn, order : 2400)
		//退出
		signOut(controller:'auth', action:'signOut', visible: loggedIn, order : 9999)
    }

	//用户
	usr{
        //主页
        home(controller:'home', action:'index', visible: loggedIn, order : 1000)
		//文件
		open(controller:'open', action:'index', visible: loggedIn, order : 2200)
		//退出
		signOut(controller:'auth', action:'signOut', visible: loggedIn, order : 9999)
	}
}