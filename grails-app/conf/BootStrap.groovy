import grails.util.GrailsUtil

import org.apache.shiro.crypto.hash.Sha512Hash
import org.apache.shiro.crypto.SecureRandomNumberGenerator

import com.cyberoller.xbi.shiro.User
import com.cyberoller.xbi.shiro.Role

import com.cyberoller.xbi.quartz2.QuartzJob

import com.cyberoller.xbi.chartjs.Dashboard
import com.cyberoller.xbi.chartjs.DataSource

import com.cyberoller.xbi.Xfolder
import com.cyberoller.xbi.Xfile

class BootStrap {

    def init = { servletContext ->
		
        switch(GrailsUtil.environment) {
            case "development":
                configureForDev()
				//configureFolderAndFile()
                break
            case "test":
                configureForDev()
				configureFolderAndFile()
            	break
			case "production":
				configureForDev()
				break
        }
    }
    def destroy = {
    }

	def configureForDev = {

		/********************************************************************************/
		def user_leaf =  User.findByUsername("leaf@xbi".trim().toUpperCase())
		if(!user_leaf){
			String salt = new SecureRandomNumberGenerator().nextBytes().toHex()	 //生成随机数

			user_leaf = new User(
				  username: "leaf@xbi"
				, fullname : "leaf.shi"
				, passwordHash: new Sha512Hash("12345678", salt).toHex()
				, passwordSalt:salt
				, roleAlias : "ADM"
				, language : "zh_CN"
				, isActive : true
			).save(flush:true)
		}
	
		//角色
		def role_admin = Role.findByAlias("adm".trim().toUpperCase())

		if(!role_admin){
			role_admin = new Role(name:"admin", alias : "ADM", description:"角色-管理员：拥有所有权限")
				.addToPermissions("*:*")
				.addToUsers(user_leaf)
				.save(flush:true);
		}
		/********************************************************************************/
		def user_user =  User.findByUsername("user@xbi".trim().toUpperCase())
		if(!user_user){
			String salt = new SecureRandomNumberGenerator().nextBytes().toHex()	 //生成随机数

			user_user = new User(
				  username: "user@xbi"
				, fullname : "xbi.user"
				, passwordHash: new Sha512Hash("12345678", salt).toHex()
				, passwordSalt:salt
				, roleAlias : "USR"
				, language : "zh_CN"
				, isActive : true
			).save(flush:true)
		}
	
		//角色
		def role_user = Role.findByAlias("usr".trim().toUpperCase())

		if(!role_user){
			role_user = new Role(name:"user", alias : "USR", description:"角色-普通用户")
				.addToPermissions("*:*")
				.addToUsers(user_user)
				.save(flush:true);
		}
		/********************************************************************************/		
		//配置任务
		def job_report_scan = QuartzJob.findByName("ReportScanTrigger") 
		
		if(!job_report_scan)
		{
		    job_report_scan = new QuartzJob(name:"ReportScanTrigger", isActive : true).save(flush:true);
		}
	}
	def configureFolderAndFile ()  {
		def user_leaf =  User.findByUsername("leaf@xbi".trim().toUpperCase())
		//创建文件夹
		Xfolder folder_root = new Xfolder(serialNumber:"10", name : "五项管理", icon :"fa-home", description : "根目录", owner : user_leaf, createdBy : user_leaf, lastModifiedBy : user_leaf).save(flush:true)
		Xfolder folder_sales = new Xfolder(xfolder : folder_root, serialNumber:"1010", name : "销售看板", icon :"fa-tasks", description : "销售看板", owner : user_leaf, createdBy : user_leaf, lastModifiedBy : user_leaf).save(flush:true)
		Xfolder folder_account = new Xfolder(xfolder : folder_root,serialNumber:"1020", name : "客户分析", icon :"fa-users", description : "客户分析", owner : user_leaf, createdBy : user_leaf, lastModifiedBy : user_leaf).save(flush:true)
		Xfolder folder_work = new Xfolder(xfolder : folder_root,serialNumber:"1030", name : "工作统计", icon :"fa-suitcase", description : "工作统计", owner : user_leaf, createdBy : user_leaf, lastModifiedBy : user_leaf).save(flush:true)
		Xfolder folder_dashboard = new Xfolder(xfolder : folder_root,serialNumber:"1040", name : "仪表盘", icon :"fa-tachometer", description : "仪表盘", owner : user_leaf, createdBy : user_leaf, lastModifiedBy : user_leaf).save(flush:true)
		
		(1..5).each{ serialNumber ->
			Xfile file_report_001 = new Xfile(
				  xfolder:folder_sales
				, serialNumber : "00${serialNumber}0"
				, xtype : "Report"
				, owner : user_leaf
				, createdBy : user_leaf
				, lastModifiedBy : user_leaf
			).save(flush:true)
		}
		(1..5).each{ serialNumber ->
			Xfile file_report_001 = new Xfile(
				  xfolder:folder_account
				, serialNumber : "00${serialNumber}0"
				, xtype : "Report"
				, owner : user_leaf
				, createdBy : user_leaf
				, lastModifiedBy : user_leaf
			).save(flush:true)
		}
		(1..5).each{ serialNumber ->
			Xfile file_report_001 = new Xfile(
				  xfolder:folder_work
				, serialNumber : "00${serialNumber}0"
				, xtype : "Report"
				, owner : user_leaf
				, createdBy : user_leaf
				, lastModifiedBy : user_leaf
			).save(flush:true)
		}
		(1..5).each{ serialNumber ->
			Xfile file_report_001 = new Xfile(
				  xfolder:folder_dashboard
				, serialNumber : "00${serialNumber}0"
				, xtype : "Dashboard"
				, owner : user_leaf
				, createdBy : user_leaf
				, lastModifiedBy : user_leaf
			).save(flush:true)
		}
		/********************************************************************************/
		//创建仪表盘Datasource
		def ds_as = DataSource.findByName("as".toLowerCase())
		if(!ds_as){
			ds_as = new DataSource(
			  name : "as"
			, url : "jdbc:mysql://192.168.3.246:3306/shwx?useUnicode=true&characterEncoding=UTF-8"
			, username :"root"
			, password :"0Yishuihan;"
			, driver : "com.mysql.jdbc.Driver"
			, description : "傲胜"
			, owner : user_leaf
			, createdBy : user_leaf
			, lastModifiedBy : user_leaf
			).save(flush:true)
		}
		def ds_home = DataSource.findByName("home".toLowerCase())
		if(!ds_home){
			ds_home = new DataSource(
			  name : "home"
			, url : "jdbc:mysql://127.0.0.1:3306/shwx?useUnicode=true&characterEncoding=UTF-8"
			, username :"root"
			, password :"123456"
			, driver : "com.mysql.jdbc.Driver"
			, description : "本机"
			, owner : user_leaf
			, createdBy : user_leaf
			, lastModifiedBy : user_leaf
			).save(flush:true)
		}
		/********************************************************************************/
		def dashboard_snapshot = Dashboard.findByName ("snapshot".toLowerCase())
		if(!dashboard_snapshot)
		{
		    dashboard_snapshot = new Dashboard(
		    	  name:"snapshot"
		    	, author : "leaf@xbi"
		    	, title : "快照"
		    	, datasource : ds_home
		    	, description : "demo"
		    	, atMenu : true
		    	, menuOrder : "10"
		    	, homePage : true
		    	, isActive : true
				, owner : user_leaf
				, createdBy : user_leaf
				, lastModifiedBy : user_leaf
		    ).save(flush:true)
		}
		/********************************************************************************/
		def dashboard_product = Dashboard.findByName ("product".toLowerCase())  
		if(!dashboard_product)
		{
		    dashboard_product = new Dashboard(
		    	  name:"product"
		    	, author : "leaf@xbi"
		    	, title : "产品"
		    	, datasource : ds_home
		    	, description : "demo"
		    	, atMenu : true
		    	, menuOrder : "20"
		    	, homePage : false
		    	, isActive : true
				, owner : user_leaf
				, createdBy : user_leaf
				, lastModifiedBy : user_leaf
		    ).save(flush:true)
		}
		/********************************************************************************/
		def dashboard_region = Dashboard.findByName ("region".toLowerCase())  
		if(!dashboard_region)
		{
		    dashboard_region = new Dashboard(
		    	  name:"region"
		    	, author : "leaf@xbi"
		    	, title : "地区"
		    	, datasource : ds_home
		    	, description : "地区"
		    	, atMenu : true
		    	, menuOrder : "30"
		    	, homePage : false
		    	, isActive : true
				, owner : user_leaf
				, createdBy : user_leaf
				, lastModifiedBy : user_leaf
		    ).save(flush:true)
		}

	}
}