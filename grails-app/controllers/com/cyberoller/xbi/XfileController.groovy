package com.cyberoller.xbi

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import com.cyberoller.xbi.birt.ReportScanService
import com.cyberoller.xbi.birt.Report

import com.cyberoller.xbi.chartjs.Dashboard
import com.cyberoller.xbi.chartjs.DataSource

@Transactional(readOnly = true)
class XfileController {

	def xfileService

	def birtReportService
	def reportScanService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Xfile.list(params), model:[xfileInstanceCount: Xfile.count()], view : '/xfile/index'
    }

    def show(Xfile xfileInstance) {
        respond xfileInstance
    }

    def create() {
		try{
			def currentUserId = xfileService.currentUserId()
			respond new Xfile(params), view : '/xfile/create', model : [currentUserId : currentUserId]
		}catch(e){
			flash.message = e.getMessage()
			redirect action:"index", method:"GET"
		}
    }

    @Transactional
    def save(Xfile xfileInstance) {
        if (xfileInstance == null) {
            notFound()
            return
        }

        if (xfileInstance.hasErrors()) {
            respond xfileInstance.errors, view:'create'
            return
        }

        xfileInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'xfileInstance.label', default: 'Xfile'), xfileInstance.id])
                redirect xfileInstance
            }
            '*' { respond xfileInstance, [status: CREATED] }
        }
    }

    def edit(Xfile xfileInstance) {
    	def currentUserId = xfileService.currentUserId()
        respond xfileInstance, model : [currentUserId : currentUserId]
    }

    @Transactional
    def update(Xfile xfileInstance) {
        if (xfileInstance == null) {
            notFound()
            return
        }

        if (xfileInstance.hasErrors()) {
            respond xfileInstance.errors, view:'edit'
            return
        }

        xfileInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Xfile.label', default: 'Xfile'), xfileInstance.id])
                redirect xfileInstance
            }
            '*'{ respond xfileInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Xfile xfileInstance) {

        if (xfileInstance == null) {
            notFound()
            return
        }

        xfileInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Xfile.label', default: 'Xfile'), xfileInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'xfileInstance.label', default: 'Xfile'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

	/*上传*/
	@Transactional(readOnly=false)
    def upload(Long id) {
		try{
			def source = request.getFile("xfile")
			
			if(source.isEmpty()){
				throw new RuntimeException("空文件！")
			}

			def xfileInstance = Xfile.withCriteria(uniqueResult:true){
				eq("id", id)
			}

			switch(xfileInstance.xtype){
				case "Report":
					def birtHome = birtReportService.reportHome
					String xfile_name = source.getOriginalFilename()//获得文件原始的名称
			
					def xfile = new File("${birtHome}/${xfile_name}")
					if(xfile.exists()){
						xfile.delete()//删除旧文件
					}
					if(!xfile.exists()){
						xfile.mkdirs()//目录不存在则创建
					}
					source.transferTo( xfile )
					//扫描
					reportScanService.scan(xfileInstance.owner.id)

					def reportInstance = Report.withCriteria(uniqueResult:true){
						eq("filename", xfile_name)
					}

					xfileInstance.report = reportInstance
					xfileInstance.save(flush:true)
					break;
				case "Dashboard":
					def birtHome = birtReportService.reportHome
					String xfile_name = "${xfileInstance.serialNumber}.txt"//获得文件原始的名称
			
					def xfile = new File("${birtHome}/${xfile_name}")
					if(xfile.exists()){
						xfile.delete()//删除旧文件
					}
					if(!xfile.exists()){
						xfile.mkdirs()//目录不存在则创建
					}
					source.transferTo( xfile )
					//新增仪表盘
					def dashboardInstance = Dashboard.withCriteria(uniqueResult:true){
						eq("name", xfileInstance.serialNumber)
					}
					if(!dashboardInstance){
						dashboardInstance = new Dashboard(
							  name:xfileInstance.serialNumber
							, author : "leaf@xbi"
							, title : xfileInstance.serialNumber
							, datasource : DataSource.get(1)
							, description : xfileInstance.serialNumber
							, isActive : true
							, owner : xfileInstance.owner
							, createdBy : xfileInstance.createdBy
							, lastModifiedBy : xfileInstance.lastModifiedBy
						).save(flush:true)
					}
					
					xfileInstance.dashboard = dashboardInstance
					xfileInstance.save(flush:true)
					break;
			}
		}catch(e){
			flash.message = e.getMessage()
		}
		redirect action:"show", id: id, method:"GET"
    }
}
