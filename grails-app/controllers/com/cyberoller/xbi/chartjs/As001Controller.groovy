package com.cyberoller.xbi.chartjs

class As001Controller {

	//def as001Service

	/*nav*/
    def index(){
		render view : "/chartjs/dashboard/as001/index"
	}

	def products ()  {
	    render view : "/chartjs/dashboard/as001/products"
	}

    def sectors() { 
		render view : "/chartjs/dashboard/as001/sectors"
	}

    def regions() { 
		render view : "/chartjs/dashboard/as001/regions"
	}

    def channels() { 
		render view : "/chartjs/dashboard/as001/channels"
	}
	/*load page*/
	def page ()  {
	    render template : "/chartjs/dashboard/as001/${params.id}"
	}
	/*load data*/
	def sales (){
			if (params?.startDate && params.endDate) {
			render(contentType:"text/json") {
				array{
					"0"(SaleDate:"2014-01-01T00:00:00",Sales:1077700.0)
					"1"(SaleDate:"2014-01-02T00:00:00",Sales:1105350.0)
					"2"(SaleDate:"2014-01-03T00:00:00",Sales:1067100.0)
					"3"(SaleDate:"2014-01-04T00:00:00",Sales:1070100.0)
					"4"(SaleDate:"2014-01-05T00:00:00",Sales:1075600.0)
					"5"(SaleDate:"2014-01-06T00:00:00",Sales:1033600.0)
					"6"(SaleDate:"2014-01-07T00:00:00",Sales:1112550.0)
					"7"(SaleDate:"2014-01-08T00:00:00",Sales:1059350.0)
					"8"(SaleDate:"2014-01-09T00:00:00",Sales:1111700.0)
					"9"(SaleDate:"2014-01-10T00:00:00",Sales:1298350.0)
					"10"(SaleDate:"2014-01-11T00:00:00",Sales:1181300.0)
					"11"(SaleDate:"2014-01-12T00:00:00",Sales:1296800.0)
					"12"(SaleDate:"2014-01-13T00:00:00",Sales:1292550.0)
					"13"(SaleDate:"2014-01-14T00:00:00",Sales:1252300.0)
					"14"(SaleDate:"2014-01-15T00:00:00",Sales:1307250.0)
					"15"(SaleDate:"2014-01-16T00:00:00",Sales:1401900.0)
					"16"(SaleDate:"2014-01-17T00:00:00",Sales:1286100.0)
					"17"(SaleDate:"2014-01-18T00:00:00",Sales:1369550.0)
					"18"(SaleDate:"2014-01-19T00:00:00",Sales:1335200.0)
					"19"(SaleDate:"2014-01-20T00:00:00",Sales:1222800.0)
					"20"(SaleDate:"2014-01-21T00:00:00",Sales:1268100.0)
					"21"(SaleDate:"2014-01-22T00:00:00",Sales:1370000.0)
					"22"(SaleDate:"2014-01-23T00:00:00",Sales:1256300.0)
					"23"(SaleDate:"2014-01-24T00:00:00",Sales:1270250.0)
					"24"(SaleDate:"2014-01-25T00:00:00",Sales:1120400.0)
					"25"(SaleDate:"2014-01-26T00:00:00",Sales:1225650.0)
					"26"(SaleDate:"2014-01-27T00:00:00",Sales:1183300.0)
					"27"(SaleDate:"2014-01-28T00:00:00",Sales:1211850.0)
					"28"(SaleDate:"2014-01-29T00:00:00",Sales:1316450.0)
					"29"(SaleDate:"2014-01-30T00:00:00",Sales:1188900.0)
					"30"(SaleDate:"2014-01-31T00:00:00",Sales:1239550.0)
					"31"(SaleDate:"2014-02-01T00:00:00",Sales:1191150.0)
					"32"(SaleDate:"2014-02-02T00:00:00",Sales:1085700.0)
					"33"(SaleDate:"2014-02-03T00:00:00",Sales:1128000.0)
					"34"(SaleDate:"2014-02-04T00:00:00",Sales:1025250.0)
					"35"(SaleDate:"2014-02-05T00:00:00",Sales:978600.0)
					"36"(SaleDate:"2014-02-06T00:00:00",Sales:1063150.0)
					"37"(SaleDate:"2014-02-07T00:00:00",Sales:1119650.0)
					"38"(SaleDate:"2014-02-08T00:00:00",Sales:1026050.0)
					"39"(SaleDate:"2014-02-09T00:00:00",Sales:1036900.0)
					"40"(SaleDate:"2014-02-10T00:00:00",Sales:1193800.0)
					"41"(SaleDate:"2014-02-11T00:00:00",Sales:1215350.0)
					"42"(SaleDate:"2014-02-12T00:00:00",Sales:1308850.0)
					"43"(SaleDate:"2014-02-13T00:00:00",Sales:1227500.0)
					"44"(SaleDate:"2014-02-14T00:00:00",Sales:1193400.0)
					"45"(SaleDate:"2014-02-15T00:00:00",Sales:1333950.0)
					"46"(SaleDate:"2014-02-16T00:00:00",Sales:1271950.0)
					"47"(SaleDate:"2014-02-17T00:00:00",Sales:1153550.0)
					"48"(SaleDate:"2014-02-18T00:00:00",Sales:1276900.0)
					"49"(SaleDate:"2014-02-19T00:00:00",Sales:1276400.0)
					"50"(SaleDate:"2014-02-20T00:00:00",Sales:1173550.0)
					"51"(SaleDate:"2014-02-21T00:00:00",Sales:1252100.0)
					"52"(SaleDate:"2014-02-22T00:00:00",Sales:1244100.0)
					"53"(SaleDate:"2014-02-23T00:00:00",Sales:1244550.0)
					"54"(SaleDate:"2014-02-24T00:00:00",Sales:1277450.0)
					"55"(SaleDate:"2014-02-25T00:00:00",Sales:1253200.0)
					"56"(SaleDate:"2014-02-26T00:00:00",Sales:1155000.0)
					"57"(SaleDate:"2014-02-27T00:00:00",Sales:1160500.0)
					"58"(SaleDate:"2014-02-28T00:00:00",Sales:1187350.0)
					"59"(SaleDate:"2014-03-01T00:00:00",Sales:1043050.0)
					"60"(SaleDate:"2014-03-02T00:00:00",Sales:1031500.0)
					"61"(SaleDate:"2014-03-03T00:00:00",Sales:956350.0)
					"62"(SaleDate:"2014-03-04T00:00:00",Sales:1076650.0)
					"63"(SaleDate:"2014-03-05T00:00:00",Sales:1206100.0)
					"64"(SaleDate:"2014-03-06T00:00:00",Sales:999150.0)
					"65"(SaleDate:"2014-03-07T00:00:00",Sales:1099250.0)
					"66"(SaleDate:"2014-03-08T00:00:00",Sales:1154800.0)
					"67"(SaleDate:"2014-03-09T00:00:00",Sales:1047050.0)
					"68"(SaleDate:"2014-03-10T00:00:00",Sales:1295400.0)
					"69"(SaleDate:"2014-03-11T00:00:00",Sales:1267300.0)
					"70"(SaleDate:"2014-03-12T00:00:00",Sales:1247650.0)
					"71"(SaleDate:"2014-03-13T00:00:00",Sales:1233000.0)
					"72"(SaleDate:"2014-03-14T00:00:00",Sales:1157050.0)
					"73"(SaleDate:"2014-03-15T00:00:00",Sales:1293000.0)
					"74"(SaleDate:"2014-03-16T00:00:00",Sales:1211500.0)
					"75"(SaleDate:"2014-03-17T00:00:00",Sales:1191450.0)
					"76"(SaleDate:"2014-03-18T00:00:00",Sales:1319600.0)
					"77"(SaleDate:"2014-03-19T00:00:00",Sales:1318900.0)
					"78"(SaleDate:"2014-03-20T00:00:00",Sales:1279650.0)
					"79"(SaleDate:"2014-03-21T00:00:00",Sales:1266600.0)
					"80"(SaleDate:"2014-03-22T00:00:00",Sales:1295500.0)
					"81"(SaleDate:"2014-03-23T00:00:00",Sales:1145550.0)
					"82"(SaleDate:"2014-03-24T00:00:00",Sales:1311200.0)
					"83"(SaleDate:"2014-03-25T00:00:00",Sales:1317950.0)
					"84"(SaleDate:"2014-03-26T00:00:00",Sales:1232450.0)
					"85"(SaleDate:"2014-03-27T00:00:00",Sales:1138950.0)
					"86"(SaleDate:"2014-03-28T00:00:00",Sales:1274600.0)
					"87"(SaleDate:"2014-03-29T00:00:00",Sales:1284500.0)
					"88"(SaleDate:"2014-03-30T00:00:00",Sales:1197450.0)
					"89"(SaleDate:"2014-03-31T00:00:00",Sales:1209750.0)
					"90"(SaleDate:"2014-04-01T00:00:00",Sales:975550.0)
					"91"(SaleDate:"2014-04-02T00:00:00",Sales:963550.0)
					"92"(SaleDate:"2014-04-03T00:00:00",Sales:1094750.0)
					"93"(SaleDate:"2014-04-04T00:00:00",Sales:998150.0)
					"94"(SaleDate:"2014-04-05T00:00:00",Sales:1168500.0)
					"95"(SaleDate:"2014-04-06T00:00:00",Sales:1094350.0)
					"96"(SaleDate:"2014-04-07T00:00:00",Sales:1135200.0)
					"97"(SaleDate:"2014-04-08T00:00:00",Sales:997550.0)
					"98"(SaleDate:"2014-04-09T00:00:00",Sales:1078800.0)
					"99"(SaleDate:"2014-04-10T00:00:00",Sales:1225100.0)
					"100"(SaleDate:"2014-04-11T00:00:00",Sales:1318550.0)
					"101"(SaleDate:"2014-04-12T00:00:00",Sales:1319400.0)
					"102"(SaleDate:"2014-04-13T00:00:00",Sales:1193700.0)
					"103"(SaleDate:"2014-04-14T00:00:00",Sales:1318750.0)
					"104"(SaleDate:"2014-04-15T00:00:00",Sales:1236650.0)
					"105"(SaleDate:"2014-04-16T00:00:00",Sales:1332100.0)
					"106"(SaleDate:"2014-04-17T00:00:00",Sales:1264500.0)
					"107"(SaleDate:"2014-04-18T00:00:00",Sales:1218250.0)
					"108"(SaleDate:"2014-04-19T00:00:00",Sales:1256600.0)
					"109"(SaleDate:"2014-04-20T00:00:00",Sales:1231350.0)
					"110"(SaleDate:"2014-04-21T00:00:00",Sales:1244200.0)
					"111"(SaleDate:"2014-04-22T00:00:00",Sales:1125400.0)
					"112"(SaleDate:"2014-04-23T00:00:00",Sales:1162350.0)
					"113"(SaleDate:"2014-04-24T00:00:00",Sales:1168700.0)
					"114"(SaleDate:"2014-04-25T00:00:00",Sales:1198500.0)
					"115"(SaleDate:"2014-04-26T00:00:00",Sales:1204450.0)
					"116"(SaleDate:"2014-04-27T00:00:00",Sales:1351850.0)
				}
			}
		}else if (params?.day) {
			render(contentType:"text/json") {
				array{
					"0"(SaleDate:	"2014-04-01T08:00:00", Sales:975550)
					"1"(SaleDate:	"2014-04-01T09:00:00", Sales:963550)
					"2"(SaleDate:	"2014-04-01T10:00:00", Sales:1094750)
					"3"(SaleDate:	"2014-04-01T11:00:00", Sales:998150)
					"4"(SaleDate:	"2014-04-01T12:00:00", Sales:1168500)
					"5"(SaleDate:	"2014-04-01T13:00:00", Sales:1094350)
				}
			}
		}else if (params?.month){
			render(contentType:"text/json") {
				array{
					"0"(SaleDate:	"2014-04-01T00:00:00", Sales:975550)
					"1"(SaleDate:	"2014-04-02T00:00:00", Sales:963550)
					"2"(SaleDate:	"2014-04-03T00:00:00", Sales:1094750)
					"3"(SaleDate:	"2014-04-04T00:00:00", Sales:998150)
					"4"(SaleDate:	"2014-04-05T00:00:00", Sales:1168500)
					"5"(SaleDate:	"2014-04-06T00:00:00", Sales:1094350)
				}
			}
		} else{
			render(contentType:"text/json") {
				DailyPerformance(TodaySales:0, YesterdaySales:1125400, LastWeekSales: 8820550)
				MonthlyPerformance(ThisMonthSales:	25790950, LastMonthSales:37102900, YTDSales:133555650)
				AnnualPerformance(YTDSales:	133555650, ForecastSales:	435559422.9165164, LastYearSales : 434180400)
				SalesBySector = array{
					"0"(Criteria:"Telecom", Sales:198480400, Units:101564)
					"1"(Criteria:"Insurance", Sales:132846950, Units:67906)
					"2"(Criteria:"Manufacturing", Sales:201035950, Units:102836)
					"3"(Criteria:"Energy", Sales:333208750, Units:170044)
					"4"(Criteria:"Banking", Sales:65637600, Units:33503)
					"5"(Criteria:"Health", Sales:66952200, Units:34212)
				}
			}
		}
	}

	def product (){
		if (params?.startDate && params.endDate) {
			render(contentType:"text/json") {
				array{
					"0"(Criteria:"实木地板",Sales:254874,Units:866)
					"1"(Criteria:"强化地板",Sales:448935,Units:3365)
				}
			}
		}else if (params?.month){
			render(contentType:"text/json") {
				array{
					"0"(Criteria:"实木地板",Sales:254874,Units:866)
					"1"(Criteria:"强化地板",Sales:448935,Units:3365)
				}
			}
		}else if(params?.day){
			render(contentType:"text/json") {
				array{
					"0"(Criteria:"实木地板",Sales:254874,Units:866)
					"1"(Criteria:"强化地板",Sales:448935,Units:3365)
				}
			}
		} else{
			render(contentType:"text/json") {
				[TodaySales:100000
				,YesterdaySales:120000
				,LastWeekSales:1234567
				,ThisMonthUnits:0
				,LastMonthUnits:149 + 567
				,YtdUnits:305 + 1114]
			}
		}
	}

	def sector (){
		if (params?.startDate && params.endDate) {
			render(contentType:"text/json") {
				array{
					"0"(Criteria:"Banking",Sales:8973700.0,Units:4609)
					"1"(Criteria:"Energy",Sales:47204300.0,Units:24017)
					"2"(Criteria:"Health",Sales:9013450.0,Units:4638)
					"3"(Criteria:"Insurance",Sales:18567250.0,Units:9465)
					"4"(Criteria:"Manufacturing",Sales:28375350.0,Units:14465)
					"5"(Criteria:"Telecom",Sales:27507450.0,Units:14102)
				}
			}
		}else if (params?.month){
			render(contentType:"text/json") {
				array{
					"0"(Criteria:"Banking",Sales:2077500.0,Units:1052)
					"1"(Criteria:"Energy",Sales:10656400.0,Units:5421)
					"2"(Criteria:"Health",Sales:2229600.0,Units:1142)
					"3"(Criteria:"Insurance",Sales:4101750.0,Units:2101)
					"4"(Criteria:"Manufacturing",Sales:6519200.0,Units:3337)
					"5"(Criteria:"Telecom",Sales:6292350.0,Units:3233)
				}
			}
		}else if(params?.day){
			render(contentType:"text/json") {
				array{
					"0"(Criteria:"Banking",Sales:98400.0,Units:49)
					"1"(Criteria:"Energy",Sales:410350.0,Units:205)
					"2"(Criteria:"Health",Sales:99450.0,Units:49)
					"3"(Criteria:"Insurance",Sales:129500.0,Units:69)
					"4"(Criteria:"Manufacturing",Sales:275450.0,Units:133)
					"5"(Criteria:"Telecom",Sales:338700.0,Units:163)
				}
			}
		} else{
			render(contentType:"text/json") {
				[TodaySales:0.0,YesterdaySales:1351850.0,LastWeekSales:8334950.0,ThisMonthUnits:16286.0,LastMonthUnits:18907.0,YtdUnits:71296.0]
			}
		}
	}

	def region (){
		if (params?.startDate && params.endDate) {
			render(contentType:"text/json") {
				array{
					"0"(Criteria:"工程部(大客户部)",Sales:98400.0,Units:49)
					"1"(Criteria:"销售渠道南区",Sales:410350.0,Units:205)
					"2"(Criteria:"天津分公司",Sales:99450.0,Units:49)
					"3"(Criteria:"上海直营",Sales:129500.0,Units:69)
					"4"(Criteria:"市场部",Sales:275450.0,Units:133)
					"5"(Criteria:"产品管理部",Sales:338700.0,Units:163)
				}
			}
		}else if (params?.month){
			render(contentType:"text/json") {
				array{
					"0"(Criteria:"工程部(大客户部)",Sales:98400.0,Units:49)
					"1"(Criteria:"销售渠道南区",Sales:410350.0,Units:205)
					"2"(Criteria:"天津分公司",Sales:99450.0,Units:49)
					"3"(Criteria:"上海直营",Sales:129500.0,Units:69)
					"4"(Criteria:"市场部",Sales:275450.0,Units:133)
					"5"(Criteria:"产品管理部",Sales:338700.0,Units:163)
				}
			}
		}else if(params?.day){
			render(contentType:"text/json") {
				array{
					"0"(Criteria:"工程部(大客户部)",Sales:98400.0,Units:49)
					"1"(Criteria:"销售渠道南区",Sales:410350.0,Units:205)
					"2"(Criteria:"天津分公司",Sales:99450.0,Units:49)
					"3"(Criteria:"上海直营",Sales:129500.0,Units:69)
					"4"(Criteria:"市场部",Sales:275450.0,Units:133)
					"5"(Criteria:"产品管理部",Sales:338700.0,Units:163)
				}
			}
		} else{
			render(contentType:"text/json") {
				[TodaySales:0.0,YesterdaySales:1351850.0,LastWeekSales:8334950.0,ThisMonthUnits:16286.0,LastMonthUnits:18907.0,YtdUnits:71296.0]
			}
		}
	}

	def channel (){
		if (params?.startDate && params.endDate) {
			render(contentType:"text/json") {
				array{
					"0"(Criteria:"Consultants",Sales:16920900.0,Units:8667)
					"1"(Criteria:"Direct",Sales:43415500.0,Units:22183)
					"2"(Criteria:"Resellers",Sales:26443750.0,Units:13520)
					"3"(Criteria:"Retail",Sales:34816950.0,Units:17745)
					"4"(Criteria:"VARs",Sales:18044400.0,Units:9181)
				}
			}
		}else if (params?.month){
			render(contentType:"text/json") {
				array{
					"0"(Criteria:"Banking",Sales:2077500.0,Units:1052)
					"1"(Criteria:"Energy",Sales:10656400.0,Units:5421)
					"2"(Criteria:"Health",Sales:2229600.0,Units:1142)
					"3"(Criteria:"Insurance",Sales:4101750.0,Units:2101)
					"4"(Criteria:"Manufacturing",Sales:6519200.0,Units:3337)
					"5"(Criteria:"Telecom",Sales:6292350.0,Units:3233)
				}
			}
		}else if(params?.day){
			render(contentType:"text/json") {
				array{
					"0"(SaleDate:"2014-04-27T08:00:00",SalesByChannel:[Direct:22900.0,Retail:21500.0,Consultants:11350.0,VARs:8600.0,Resellers:13600.0])
					"1"(SaleDate:"2014-04-27T09:00:00",SalesByChannel:[VARs:12800.0,Consultants:10750.0,Retail:21100.0,Direct:40700.0,Resellers:7800.0])
					"2"(SaleDate:"2014-04-27T10:00:00",SalesByChannel:[Direct:10500.0,Consultants:11500.0,VARs:32700.0,Retail:33900.0,Resellers:16900.0])
					"3"(SaleDate:"2014-04-27T11:00:00",SalesByChannel:[Consultants:38150.0,Retail:21500.0,Direct:31350.0,VARs:23500.0,Resellers:16800.0])
					"4"(SaleDate:"2014-04-27T12:00:00",SalesByChannel:[Direct:88200.0,Retail:37350.0,VARs:7000.0,Consultants:11700.0,Resellers:19900.0])
					"5"(SaleDate:"2014-04-27T13:00:00",SalesByChannel:[VARs:28950.0,Retail:34550.0,Direct:75000.0,Resellers:42900.0,Consultants:16650.0])
					"6"(SaleDate:"2014-04-27T14:00:00",SalesByChannel:[Consultants:29400.0,Resellers:53000.0,Direct:53750.0,Retail:24250.0,VARs:31550.0])
					"7"(SaleDate:"2014-04-27T15:00:00",SalesByChannel:[Direct:39000.0,Resellers:52400.0,Retail:91100.0,Consultants:18400.0,VARs:17600.0])
					"8"(SaleDate:"2014-04-27T16:00:00",SalesByChannel:[Direct:34000.0,VARs:13000.0,Consultants:29950.0,Resellers:17150.0,Retail:18750.0])
					"9"(SaleDate:"2014-04-27T17:00:00",SalesByChannel:[Resellers:7400.0,Retail:10700.0,Direct:21150.0,VARs:10700.0,Consultants:8500.0])
				}
			}
		}
	}
}
