package com.cyberoller.xbi.chartjs



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class DataSourceController {

	def dataSourceService
	def currentUserId

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

	def beforeInterceptor = {
		currentUserId = dataSourceService.currentUserId()
	}

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond DataSource.list(params), model:[dataSourceInstanceCount: DataSource.count()], view : "/chartjs/dataSource/index"
    }

    def show(DataSource dataSourceInstance) {
        respond dataSourceInstance, view : "/chartjs/dataSource/show"
    }

    def create() {
        respond new DataSource(params), view : "/chartjs/dataSource/create", model : [currentUserId : currentUserId]
    }

    @Transactional
    def save(DataSource dataSourceInstance) {
        if (dataSourceInstance == null) {
            notFound()
            return
        }

        if (dataSourceInstance.hasErrors()) {
            respond dataSourceInstance.errors, view:'/chartjs/dataSource/create'
            return
        }

        dataSourceInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'dataSourceInstance.label', default: 'DataSource'), dataSourceInstance.id])
                redirect dataSourceInstance
            }
            '*' { respond dataSourceInstance, [status: CREATED] }
        }
    }

    def edit(DataSource dataSourceInstance) {
        respond dataSourceInstance, view : "/chartjs/dataSource/edit", model : [currentUserId : currentUserId]
    }

    @Transactional
    def update(DataSource dataSourceInstance) {
        if (dataSourceInstance == null) {
            notFound()
            return
        }

        if (dataSourceInstance.hasErrors()) {
            respond dataSourceInstance.errors, view:'/chartjs/dataSource/edit'
            return
        }

        dataSourceInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'DataSource.label', default: 'DataSource'), dataSourceInstance.id])
                redirect dataSourceInstance
            }
            '*'{ respond dataSourceInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(DataSource dataSourceInstance) {

        if (dataSourceInstance == null) {
            notFound()
            return
        }

        dataSourceInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'DataSource.label', default: 'DataSource'), dataSourceInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'dataSourceInstance.label', default: 'DataSource'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def testConnection(Long id) {
		def result = ""
        try{
			dataSourceService.testConnection(id)
			result = "Okay!"
		}catch(e){
			result = e.getMessage()
		}
		render result
		return
    }
}
