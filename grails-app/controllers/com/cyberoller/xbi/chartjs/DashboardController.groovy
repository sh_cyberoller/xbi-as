package com.cyberoller.xbi.chartjs

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import grails.converters.JSON

@Transactional(readOnly = true)
class DashboardController {

	def dashboardService
	def currentUserId

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

	def beforeInterceptor = {
		currentUserId = dashboardService.currentUserId()
	}

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond view : "/chartjs/dashboard/index", Dashboard.list(params), model:[ dashboardInstanceCount: Dashboard.count()]
    }

    def show(Dashboard dashboardInstance) {
        respond view : "/chartjs/dashboard/show", dashboardInstance
    }

    def create() {
        respond new Dashboard(params), view : "/chartjs/dashboard/create", model : [currentUserId : currentUserId]
    }

    @Transactional
    def save(Dashboard dashboardInstance) {
        if (dashboardInstance == null) {
            notFound()
            return
        }

        if (dashboardInstance.hasErrors()) {
            respond dashboardInstance.errors, view:'/chartjs/dashboard/create', model : [currentUserId : currentUserId]
            return
        }

        dashboardInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'dashboardInstance.label', default: 'Dashboard'), dashboardInstance.id])
                redirect dashboardInstance
            }
            '*' { respond dashboardInstance, [status: CREATED] }
        }
    }

    def edit(Dashboard dashboardInstance) {
        respond dashboardInstance, view : '/chartjs/dashboard/edit', model : [currentUserId : currentUserId]
    }

    @Transactional
    def update(Dashboard dashboardInstance) {
        if (dashboardInstance == null) {
            notFound()
            return
        }

        if (dashboardInstance.hasErrors()) {
            respond dashboardInstance.errors, view:'/chartjs/dashboard/edit', model : [currentUserId : currentUserId]
            return
        }

        dashboardInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Dashboard.label', default: 'Dashboard'), dashboardInstance.id])
                redirect dashboardInstance
            }
            '*'{ respond dashboardInstance, [status: OK] }
        }
    }

    protected void notFound() {
		log.info "*******************************not found"
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'dashboardInstance.label', default: 'Dashboard'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

	/*运行*/
	def run(Dashboard dashboardInstance) {
		try {
			respond view : "/chartjs/dashboard/run", dashboardInstance
		} catch(Exception e){
			flash.error = "Dashboard ${id} encountered an error: ${e.message}"
			redirect(action:"${params.action}")
		}
        return
    }

	/*获取模板*/
	def template (Long id) {
		def content = ""
		try{
			//獲取目錄
			String applicationPath = request.getSession().getServletContext().getRealPath("")
			def dashboardHome = "${applicationPath}/xbi/dashboard"
			content = dashboardService.template(id, dashboardHome)
		}catch(e){
			response.status = 500
			content = e.getMessage()
		}
	    render content
		return
	}
	/*获取数据*/
	def data (Long id, String sql) {
		def data = ""
		try{
			data = dashboardService.data(id, sql)
		}catch(e){
			response.status = 500
			data = e.getMessage()
		}
	    render data
		return
	}
	
	/*上传*/
	@Transactional(readOnly=false)
    def upload(Long id) {
		try{
			//獲取文件
			def source = request.getFile("xfile")
			//判斷文件大小
			if(source.isEmpty()){
				throw new RuntimeException("空文件！")
			}
			//獲取儀表盤實例
			def dashboardInstance = Dashboard.withCriteria(uniqueResult:true){
				eq("id", id)
			}
			if(!dashboardInstance){
				throw new RuntimeException("获取仪表盘实例${id}错误")
			}
			//獲取目錄
			String applicationPath = request.getSession().getServletContext().getRealPath("")
			def dashboardHome = "${applicationPath}/xbi/dashboard"
			
			String xfile_name = "${dashboardInstance.name}.html"//获得文件原始的名称
			
			def xfile = new File("${dashboardHome}/${xfile_name}")
			if(xfile.exists()){
				xfile.delete()//删除旧文件
			}
			if(!xfile.exists()){
				xfile.mkdirs()//目录不存在则创建
			}
			source.transferTo( xfile )

		}catch(e){
			flash.message = e.getMessage()
		}
		redirect action:"show", id: id, method:"GET"
    }
}