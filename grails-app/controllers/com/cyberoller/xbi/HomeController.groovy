package com.cyberoller.xbi

class HomeController {

	def homeService
	def roleAlias
	
	def beforeInterceptor = {
		roleAlias = homeService.roleAlias()
	}

	/*nav*/
    def index() {
    	switch(roleAlias.toUpperCase()){
    		case "USR" :
    			def dashboardInstance = homeService.homeDashboard();
    			log.debug("主页仪表盘：${dashboardInstance}")
    			if(dashboardInstance){
    				redirect (controller : "open", action:"dashboard", id : dashboardInstance.id)
    			}else{
    				render view : "/home/index"
    			}
    			break;
    		default :
    			render view : "/index"
    			break;
    	}
		return
	}
    /******************************B左侧菜单树*********************************************/	
	/*获取根目录*/
	def rootFolder(){
    	try{
    		render template : "/home/root", model : [rootInstance:homeService.root()]
    	}catch(e){
			render template : "/home/root", model : [errorMessage : e.getMessage()]
    	}
    	return
	}

	/*获取目录子目录*/
	def folders(){
    	try{
			def folderId = params.int('id').toLong()
    		render template : "/home/folders", model : [folderInstanceList:homeService.folders(folderId)]
    	}catch(e){
			render template : "/home/folders", model : [errorMessage : e.getMessage()]
    	}
    	return
	}

	/*获取目录文件*/
	def files(){
    	try{
			def folderId = params.int('id').toLong()
    		render template : "/home/files", model : [fileInstanceList:homeService.files(folderId)]
    	}catch(e){
    		log.debug "********error:${e.getMessage()}"
			render template : "/home/files", model : [errorMessage : e.getMessage()]
    	}
    	return	    
	}
    /******************************E左侧菜单树*********************************************/
	
	/*获取主页仪表盘*/
	def homeDashboard(){
		log.debug "****************************用户菜单"
		try{
			def dashboardInstance = homeService.homeDashboard();
			render template : "/home/userMenuHome", model : [dashboardInstance: dashboardInstance];
		}catch(e){
			flash.message = e.getMessage();
			render template : "/home/userMenuHome", model : [errorMessage : e.getMessage()];
		}
		return	
	}
	
	/*获取目录*/
	def menuList(){
		log.debug "****************************用户菜单"
		try{
			def dashboardInstanceList = homeService.menuList();
			render template : "/home/userMenuList", model : [dashboardInstanceList: dashboardInstanceList];
		}catch(e){
			flash.message = e.getMessage();
			render template : "/home/userMenuList", model : [errorMessage : e.getMessage()];
		}
		return
    }

}