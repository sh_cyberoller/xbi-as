package com.cyberoller.xbi.birt

class BirtController {

	def birtReportService
	
	//获取页面布局
	def reportService
	//配置默认参数
	def  birtConfigService

	def beforeInterceptor = {
		//删除参数
		params.remove("shiroPostRedirect")
	}

    def run = {
        if(params.id==null) {
            redirect(controller : "report", action:"list")
        } else {
            def rerun = params.remove('rerun')
            def reportName = params.remove('id')
            params.remove('controller')
            params.remove('action')
            params.remove('run')
            params.remove('submit')
			//设置当前用户ID
			params['userId'] = birtConfigService.currentUserId()

            def reportParams = params
            def missing = []
            def pars = birtReportService.getReportParams(reportName)
            if(pars==null) {
                flash.error = "Could not find the Report ${reportName}!"
                redirect(controller : "report", action:"list")
            } else {
                pars.each { p ->
                    if( !(reportParams[p.name] || p.allowBlank) ) missing << p.name
                }
                if(missing.size == 0 && rerun==null) {
                    log.debug "No parameter missing, running report"
                    redirect(action:"view", id:reportName, params:reportParams)
                } else {
                    def props = birtReportService.getReportProperties(reportName)
                    if(rerun)
                        render view : "/open/birt/run"/*"/birt/engine/run"*/, model :  [id:reportName, reportParams:reportParams, parameters:pars, title: props?.title, 'showAll':1]
                    else
                        render view : "/open/birt/run"/*"/birt/engine/run"*/, model :  [id:reportName, reportParams:reportParams, parameters:pars, title: props?.title]
                }
            }
        }
    }
    
    def view = {
        if(params.list) {
            redirect(controller : "report", action:"list")
        } else {
            def reportName=params.remove('id')
            params.remove('controller')
            params.remove('action')
            params.remove('view')
            params.remove('run')
			//设置当前用户ID
			params['userId'] = birtConfigService.currentUserId()
            HashMap reportParams = []
            // Set the values of the parameter.
            def missing = []
            def pars = birtReportService.getReportParams(reportName)
            if(pars==null) {
                flash.error = "Could not find the Report ${reportName}!"
                redirect(controller : "report", action:"list")
            } else {
                pars.each { p ->
                    switch(p.type){
                    case 4: // DateTime
                    case 7: // Date
                        if(!params[p.name]){
                            if(!p.allowBlank ){ 
                                missing << p.name
                            }
                        }else{
                            reportParams[p.name] = params[p.name]
                        }
                        break
                     case 5: // Boolean
                        if(params[p.name])
                            reportParams[p.name] = true
                        else
                            reportParams[p.name] = false
                        break
                     default:
                        reportParams[p.name] = params[p.name]
                        if( !(params[p.name] || p.allowBlank) ) missing << p.name
                    }
                }
                if(missing.size) {
                    flash.error ="Report ${params.reportName} is missing the required parameters: ${missing.join(',')}"
                    reportParams['rerun']=true
                    redirect(action:"run", id:reportName, params:reportParams)
                } else {
                    //get report name and launch the engine
                    def htmlOptions = birtReportService.getRenderOption(request, "html")
                        htmlOptions.setEmbeddable(true)
                    try {
                        def props = birtReportService.getReportProperties(reportName)
                        def reportContent = birtReportService.runAndRender(reportName, reportParams, htmlOptions).toString("UTF-8")
                        render view : "/open/birt/view"/*"/birt/engine/view"*/,contentType:'text/html', model :  [reportContent:reportContent, id:reportName, reportParams:reportParams, title : props?.title]
                        return
                    } catch(Exception e){
                        flash.error = "Report ${reportName} encountered an error: ${e.message}"
                        reportParams['rerun']=true
                        redirect(action:"run", id:reportName, params:reportParams)
                    }
                }
            }
        }
    }

    def downloadAs = {
        // get report name and launch the engine
        String reportName = params.id
        String reportExt = params.format?:'xls'
        def format = grailsApplication.config.grails.mime.types[reportExt]?reportExt:'html'
		//设置当前用户ID
		params['userId'] = birtConfigService.currentUserId()

        // Set the value of the parameter.
        HashMap reportParams = params
        reportParams.remove('action')
        reportParams.remove('controller')
		
		

        //reportParams.remove('name')
        //reportParams.remove('id')
        def options = birtReportService.getRenderOption(request, format)
        def result=birtReportService.runAndRender(reportName, reportParams, options)
        if(format=="html")
            render(view : "/open/birt/download"/*"birt/engine/download"*/, contentType:'text/html',encoding:"UTF-8", text:result.toString("UTF-8"))
        else {
            response.setHeader("Content-disposition", "attachment; filename=\"${reportName}.${reportExt}\"");
            response.contentType = grailsApplication.config.grails.mime.types[format.toLowerCase()]
            response.setCharacterEncoding "UTF-8"
            response.outputStream << result.toByteArray()
            return false
        }
    }
}