package com.cyberoller.xbi.birt

import org.springframework.dao.DataIntegrityViolationException

class ReportController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	def reportService
	def birtReportService
	def reportScanService
	def currentUserId

	def beforeInterceptor = {
		//删除参数
		params.remove("shiroPostRedirect")
		currentUserId = reportService.currentUserId()
	}

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        render view : "/birt/report/list", model : [reportInstanceList: reportService.list(params), reportInstanceTotal: reportService.count()]
    }

    def show(Long id) {
        def reportInstance = reportService.show(id)
        if (!reportInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'report.label', default: 'Report'), id])
            redirect(action: "list")
            return
        }

        render view : "/birt/report/show", model : [reportInstance: reportInstance]
    }

	def create ()  {
		render view : "/birt/report/upload"
	}

	/*上传*/
    def upload() {
		try{
			def source = request.getFile("xfile")
			
			if(source.isEmpty()){
				throw new RuntimeException("空文件！")
			}

			def birtHome = birtReportService.reportHome
			String xfile_name = source.getOriginalFilename()//获得文件原始的名称
			
			def xfile = new File("${birtHome}/${xfile_name}")
			if(xfile.exists()){
				xfile.delete()//删除旧文件
			}
			if(!xfile.exists()){
				xfile.mkdirs()//目录不存在则创建
			}
			source.transferTo( xfile )
			//扫描
			reportScanService.scan(currentUserId, xfile_name)
		}catch(e){
			flash.message = e.getMessage()
		}
		redirect action:"list", method:"GET"
    }
}