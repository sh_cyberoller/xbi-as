package com.cyberoller.xbi

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class XfolderController {

	def xfolderService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

	def currentUserId

	def beforeInterceptor = {
		//删除参数
		params.remove("shiroPostRedirect")
		currentUserId = xfolderService.currentUserId()
	}


	def fullpath(Long id){
		def fullpath = ""
		try{
			fullpath = xfolderService.fullpath(params.int('id').toLong())
		}catch(e){
			fullpath = "/e/"
		}
		render "${fullpath}"
		return
	}

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond  view : "/xfolder/index", xfolderService.list(params), model:[xfolderInstanceCount: xfolderService.count(params)]
    }

    def show(Xfolder xfolderInstance) {
		log.info "action is : xfolder.show"
        respond view : "/xfolder/show", xfolderInstance
    }

    def create() {
		try{
			def parentId = params.int('xfolder').toLong()
			def fullpath = xfolderService.fullpath(parentId)
			respond new Xfolder(params), view : "/xfolder/create", model : [fullpath : fullpath, currentUserId : currentUserId]
		}catch(e){
			flash.message = e.getMessage()
			redirect action:"index", method:"GET"
		}
    }

    @Transactional
    def save(Xfolder xfolderInstance) {
        if (xfolderInstance == null) {
            notFound()
            return
        }

        if (xfolderInstance.hasErrors()) {
			def parentId = params.int('xfolder.id').toLong()
			def fullpath = xfolderService.fullpath(parentId)
			respond xfolderInstance.errors, view:'/xfolder/create', model : [fullpath : fullpath, currentUserId : currentUserId]
            return
        }

        xfolderInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'xfolderInstance.label', default: 'Xfolder'), xfolderInstance.id])
                redirect xfolderInstance
            }
            '*' { respond xfolderInstance, [status: CREATED] }
        }
    }

    def edit(Xfolder xfolderInstance) {
		try{
			def folderId = xfolderInstance?.id?:-1L
			def fullpath = ""
			if(folderId != -1L){
				fullpath = xfolderService.fullpath(folderId)
			}else{
				fullpath = "/"
			}
			respond xfolderInstance, view : "/xfolder/edit", model : [fullpath : fullpath, currentUserId : currentUserId]
		}catch(e){
			flash.message = e.getMessage()
			redirect action:"index", method:"GET"
		}
        
    }

    @Transactional
    def update(Xfolder xfolderInstance) {
        if (xfolderInstance == null) {
            notFound()
            return
        }

        if (xfolderInstance.hasErrors()) {
			def folderId = xfolderInstance?.id?:-1L
			def fullpath = ""
			if(folderId != -1L){
				fullpath = xfolderService.fullpath(folderId)
			}else{
				fullpath = "/"
			}
			respond xfolderInstance.errors, view:'/Xfolder/edit', model : [fullpath : fullpath, currentUserId : currentUserId]
            return
        }

        xfolderInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Xfolder.label', default: 'Xfolder'), xfolderInstance.id])
                redirect xfolderInstance
            }
            '*'{ respond xfolderInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Xfolder xfolderInstance) {

        if (xfolderInstance == null) {
            notFound()
            return
        }

        xfolderInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Xfolder.label', default: 'Xfolder'), xfolderInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'xfolderInstance.label', default: 'Xfolder'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
