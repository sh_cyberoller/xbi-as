package com.cyberoller.xbi

import com.cyberoller.xbi.chartjs.Dashboard

class OpenController {

	def openService

    def index() {
    	try{
    		render view : "/open/index", model : [xfileInstanceList:openService.files()]
    	}catch(e){
			flash.message = e.getMessage()
			render view : "/open/index"
    	}
    	return
    }
    
	/*获取根目录*/
	def rootFolder(){
    	try{
    		render template : "/open/root", model : [rootInstance:openService.root()]
    	}catch(e){
			render template : "/open/root", model : [errorMessage : e.getMessage()]
    	}
    	return
	}

	/*获取目录子目录*/
	def folders(){
		log.debug "****************************foldes"
    	try{
			def folderId = params.int('id').toLong()
			log.debug "****************************folder's id is ${folderId}"
    		render template : "/open/folders", model : [folderInstanceList:openService.folders(folderId)]
    	}catch(e){
			render template : "/open/folders", model : [errorMessage : e.getMessage()]
    	}
    	return
	}

	/*获取目录文件*/
	def files(){
		log.debug "****************************files"
    	try{
			def folderId = params.int('id').toLong()
			log.debug "****************************folder's id is ${folderId}"
    		render template : "/open/files", model : [fileInstanceList:openService.files(folderId)]
    	}catch(e){
			render template : "/open/files", model : [errorMessage : e.getMessage()]
    	}
    	return	    
	}

	/*打开仪表盘*/
	def dashboard(Long id) {
		try {
			def dashboardInstance = Dashboard.get(id)
			if(!dashboardInstance){
				throw new RuntimeException("未找到仪表盘")
			}
			respond view : "/open/dashboard/run", dashboardInstance
		} catch(Exception e){
			flash.error = "Dashboard ${id} encountered an error: ${e.message}"
			redirect(action:"${params.action}")
		}
        return
    }

	/*打开仪表盘*/
	def loadDashboard(Long id) {
		try {
			def dashboardInstance = Dashboard.get(id)
			if(!dashboardInstance){
				throw new RuntimeException("未找到仪表盘")
			}
			render template : "/open/dashboard/content", model:[dashboardInstance: dashboardInstance]
		} catch(Exception e){
			render template : "/open/dashboard/content", model:[errorMessage : e.getMessage()]
		}
        return
    }
}