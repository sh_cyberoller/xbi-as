package com.cyberoller.xbi

class NavController {

	def navService

	//菜单
	def menu(){
		try{
			def roleAlias = navService.roleAlias()
			render template : "/layouts/menu", model : [menuScope: roleAlias, menuPath :params?.menuPath]
		}catch(e){
			render "菜单加载错误:${e.getMessage()}"
		}
		return
	}
}
